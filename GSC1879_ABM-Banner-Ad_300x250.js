(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"GSC1879_ABM_Banner_Ad_300x250_atlas_", frames: [[98,252,15,15],[0,0,300,250],[0,252,96,37]]}
];


// symbols:



(lib.arow = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x250_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.bg = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x250_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.logo = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x250_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhBBnIAAjJIAqAAIAIAiIADAAQAJgRAPgLQAQgKATAAQALAAAIACIgEAzQgHgCgJABQgbAAgPAOQgNANAAAXIAABng");
	this.shape.setTransform(79.9,31.3);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhKBUQgSgTAAgkIAAiDIA3AAIAAB1QAAAWAHALQAIALARAAQAWAAALgPQAKgQAAgjIAAhfIA3AAIAADJIgqAAIgIgaIgDAAQgIAPgRAHQgPAIgVAAQgjAAgSgTg");
	this.shape_1.setTransform(58.2,31.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgzBcQgWgNgMgXQgMgYAAggQAAgxAagbQAagcAtAAQAeAAAWANQAWANAMAXQAMAYAAAfQAAAxgaAcQgaAcguAAQgdAAgWgNgAgfgsQgKAPAAAdQAAAeAKAPQAKAQAVAAQAWAAAKgQQAKgPAAgeQAAgdgKgPQgKgPgWAAQgVAAgKAPg");
	this.shape_2.setTransform(34.7,31.4);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgbCEIAAhlIhYiiIA9AAIA2BtIA3htIA9AAIhYCgIAABng");
	this.shape_3.setTransform(13.6,28.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(0,0,89.4,54.4), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhOCFQgXgPAAgbQAAgSAMgMQALgMAWgGQgIgEgHgHQgGgJAAgJQAAgLAHgHQAGgIANgHQgQgHgJgQQgJgOAAgVQAAghAVgSQAWgSAnAAQAIAAAMABIAOACIBHAAIAAAcIggAIQAJAOAAARQAAAhgXASQgWASgoAAIgKgBIgIgBQgIAHAAAHQAAAMAeAAIAhAAQAiAAARAOQASAPAAAbQAAAjgeAUQgdATg3ABQgqAAgWgPgAgrBFQgKAGAAAMQAAAMALAGQALAHAUAAQAcgBARgHQARgJAAgOQAAgLgKgFQgKgEgVAAIgbAAQgQAAgKAIgAghhMQAAAQAIAJQAHAKAPAAQAPAAAHgKQAHgJAAgQQAAglgdAAQgeAAAAAlg");
	this.shape.setTransform(209.9,35.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAmBnIAAh1QAAgWgIgLQgIgKgRgBQgWAAgKAQQgKAPAAAjIAABfIg3AAIAAjJIAqAAIAHAaIADAAQAJgPARgHQAPgIAUAAQAkAAASATQASAUAAAkIAACCg");
	this.shape_1.setTransform(187.5,31.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgbCNIAAjKIA3AAIAADKgAgdhxQAAgbAdAAQAeAAAAAbQAAANgIAIQgHAGgPAAQgdAAAAgbg");
	this.shape_2.setTransform(169.7,27.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhABnIAAjJIApAAIAIAiIADAAQAKgRAOgLQARgKASAAQAMAAAGACIgDAzQgHgCgKABQgaAAgOAOQgPANABAXIAABng");
	this.shape_3.setTransform(156.7,31.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhABOQgcgbAAgxQAAgyAZgcQAagcArAAQArAAAZAZQAXAYAAArIAAAaIiBAAQABAXANAOQANANAWAAQATAAAQgEQAQgEASgIIAAArQgPAHgQADQgRAEgXAAQgwAAgbgbgAgYg1QgJALgCAUIBNAAQgBgUgKgLQgLgLgRAAQgRAAgKALg");
	this.shape_4.setTransform(136.3,31.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhABOQgcgbAAgxQAAgyAagcQAZgcArAAQAsAAAYAZQAXAYAAArIAAAaIiBAAQABAXANAOQANANAXAAQASAAAQgEQAQgEASgIIAAArQgOAHgRADQgQAEgYAAQgwAAgbgbgAgYg1QgJALgCAUIBNAAQgBgUgKgLQgLgLgRAAQgQAAgLALg");
	this.shape_5.setTransform(114.5,31.4);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAmBnIAAh1QAAgWgIgLQgIgKgRgBQgWAAgKAQQgKAPAAAjIAABfIg3AAIAAjJIAqAAIAHAaIADAAQAJgPARgHQAPgIAUAAQAkAAASATQASAUAAAkIAACCg");
	this.shape_6.setTransform(91.4,31.3);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaCNIAAjKIA2AAIAADKgAgdhxQAAgbAdAAQAeAAAAAbQAAANgIAIQgHAGgPAAQgdAAAAgbg");
	this.shape_7.setTransform(73.6,27.5);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhOCFQgXgPAAgbQAAgSAMgMQALgMAWgGQgIgEgHgHQgGgJAAgJQAAgLAHgHQAGgIANgHQgQgHgJgQQgJgOAAgVQAAghAVgSQAWgSAnAAQAIAAAMABIAOACIBHAAIAAAcIggAIQAJAOAAARQAAAhgXASQgWASgoAAIgKgBIgIgBQgIAHAAAHQAAAMAeAAIAhAAQAiAAARAOQASAPAAAbQAAAjgeAUQgdATg3ABQgqAAgWgPgAgrBFQgKAGAAAMQAAAMALAGQALAHAUAAQAcgBARgHQARgJAAgOQAAgLgKgFQgKgEgVAAIgbAAQgQAAgKAIgAghhMQAAAQAIAJQAHAKAPAAQAPAAAHgKQAHgJAAgQQAAglgdAAQgeAAAAAlg");
	this.shape_8.setTransform(57.3,35.7);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAmBnIAAh1QAAgWgIgLQgIgKgRgBQgWAAgKAQQgKAPAAAjIAABfIg3AAIAAjJIAqAAIAHAaIADAAQAJgPARgHQAPgIAUAAQAkAAASATQASAUAAAkIAACCg");
	this.shape_9.setTransform(34.9,31.3);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhLCEIAAkHICXAAIAAAuIhfAAIAAA6IBYAAIAAAsIhYAAIAABFIBfAAIAAAug");
	this.shape_10.setTransform(12.9,28.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(0,0,232.3,54.4), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgcBsQgPgRABghIAAhhIgbAAIAAgXIAegTIAQgqIAjAAIAAArIA4AAIAAApIg4AAIAABhQAAALAGAGQAHAFALAAQAOAAAUgGIAAApQgVAJgdAAQggAAgQgQg");
	this.shape.setTransform(136.9,29.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgzBcQgWgNgMgXQgMgYAAggQAAgxAagbQAagcAuAAQAcAAAXANQAWANAMAXQAMAYAAAfQAAAxgaAcQgaAcguAAQgcAAgXgNgAgfgsQgKAPAAAdQAAAeAKAPQAKAQAVAAQAXAAAJgQQAJgPAAgeQAAgdgJgPQgKgPgWAAQgVAAgKAPg");
	this.shape_1.setTransform(117.7,31.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgaCNIAAkZIA2AAIAAEZg");
	this.shape_2.setTransform(100.6,27.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgbCNIAAjKIA2AAIAADKgAgdhxQAAgbAdAAQAeAAAAAbQAAANgHAIQgIAGgPAAQgdAAAAgbg");
	this.shape_3.setTransform(89.3,27.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhaCEIAAkHIBUAAQAvAAAaAUQAYAVAAApQAAArgaAVQgbAXgwAAIgYAAIAABegAgigHIASAAQAZAAANgKQANgKAAgUQAAgUgLgJQgLgJgWAAIgZAAg");
	this.shape_4.setTransform(72.8,28.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgvAXIAAgtIBfAAIAAAtg");
	this.shape_5.setTransform(54.4,31.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgzBcQgWgNgMgXQgMgYAAggQAAgxAagbQAagcAtAAQAeAAAWANQAWANAMAXQAMAYAAAfQAAAxgaAcQgaAcguAAQgcAAgXgNgAgfgsQgKAPABAdQgBAeAKAPQAKAQAVAAQAXAAAJgQQAKgPAAgeQAAgdgKgPQgKgPgWAAQgVAAgKAPg");
	this.shape_6.setTransform(37.1,31.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhFBlQgfgjAAhBQAAgpAPgfQAPgfAcgQQAcgRAlAAQAnAAAnATIgTAtQgOgHgPgFQgPgFgPAAQgeAAgSAYQgRAXAAAqQAABYBBAAQAcAAAogOIAAAvQghAOgoAAQg5AAgegjg");
	this.shape_7.setTransform(14.2,28.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(0,0,147.3,54.4), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgmBGQgMgHAAgPQABgJAFgHQAHgIALgCQgEgCgDgEQgCgDAAgGQAAgGACgDQADgFAHgEQgIgDgFgIQgFgJgBgKQABgSAKgJQALgJATAAQAIAAAGABIAmAAIAAAKIgVADIAGAJQABAFAAAIQAAAPgKAKQgLAJgRAAIgJgBQgLAGAAAHQAAAFAEACQADABAJAAIASAAQASABAIAHQAKAHAAAOQAAASgOAJQgOAJgbAAQgVAAgLgIgAgcAhQgGAGAAAJQAAAJAIAEQAGAFAOAAQAUAAAKgGQAJgGABgLQAAgIgGgEQgGgDgOAAIgSAAQgMAAgGAFgAgTg7QgGAFAAANQAAALAGAGQAHAFALAAQAWABAAgXQAAgZgWAAQgMAAgGAHg");
	this.shape.setTransform(71.1,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_1.setTransform(59.5,18);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgHBJIAAhqIAPAAIAABqgAgGg1QgDgDABgFQgBgGADgCQADgDADAAQAEAAADADQADACAAAGQAAAFgDADQgDACgEAAQgDAAgDgCg");
	this.shape_2.setTransform(50.8,16.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_3.setTransform(42.1,18);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIBJIAAhqIAQAAIAABqgAgGg1QgCgDgBgFQABgGACgCQADgDADAAQAEAAADADQACACAAAGQAAAFgCADQgDACgEAAQgDAAgDgCg");
	this.shape_4.setTransform(33.5,16.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggAvQgKgIAAgPQAAgfAzgCIASAAIAAgIQAAgMgFgGQgFgGgMAAQgNAAgRAIIgFgMQAIgEAKgDQAJgDAJABQASAAAKAIQAJAIAAAUIAABHIgMAAIgDgPIgBAAQgIALgIADQgIAEgLgBQgQABgIgJgAAKABQgRABgJAGQgJAFAAALQAAAJAFAEQAGAFAKAAQANAAAJgIQAJgIAAgQIAAgJg");
	this.shape_5.setTransform(25,18.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgeA2IAAhqIAOAAIACAUIAAAAQAGgKAIgGQAIgFAKAAIANAAIgCAQIgMgBQgNgBgJALQgJALAAAPIAAA4g");
	this.shape_6.setTransform(16.9,18);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgIBHIAAh+IgtAAIAAgQIBqAAIAAAQIgtAAIAAB+g");
	this.shape_7.setTransform(7.6,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(0,0,78.8,31.3), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeApQgOgOgBgaQABgZANgPQAMgPAUAAQAVAAALANQAMANAAAVIAAAKIhJAAQABATAJAKQAJAJAQABQARAAARgIIAAAOIgRAGQgIACgKgBQgXAAgNgOgAgSghQgIAJgBAPIA3AAQAAgPgHgJQgHgIgNAAQgMAAgHAIg");
	this.shape.setTransform(79,18.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgeA2IAAhqIAOAAIABAUIABAAQAGgKAIgGQAIgFAKAAIANAAIgCAQIgMgBQgNgBgJALQgJALAAAPIAAA4g");
	this.shape_1.setTransform(70.4,18);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AghAvQgJgIAAgPQAAgfAzgCIASAAIAAgIQAAgMgFgGQgGgGgMAAQgMAAgRAIIgFgMQAIgEAJgDQAKgDAIABQATAAAKAIQAJAIAAAUIAABHIgMAAIgDgPIgBAAQgIALgIADQgIAEgLgBQgQABgJgJgAAKABQgRABgJAGQgJAFAAALQAAAJAGAEQAFAFAJAAQAPAAAIgIQAJgIAAgQIAAgJg");
	this.shape_2.setTransform(59.7,18.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAbA1IgUg9IgHgbIAAAAIgGAbIgVA9IgSAAIgehqIARAAIAQA9QAGAVAAAHIABAAIAEgOIAEgOIATg9IARAAIATA9QAGARACALIAAAAIADgKIAUhPIARAAIgeBqg");
	this.shape_3.setTransform(46.7,18.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgPAiIAAg/IgQAAIAAgHIAQgHIAGgXIAJAAIAAAZIAfAAIAAAMIgfAAIAAA/QAAAJAEAGQAFAEAIAAIAIAAIAHgCIAAANQgDABgFABIgJABQgeAAAAghg");
	this.shape_4.setTransform(35.4,16.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgRBMIAAhdIgTAAIAAgHIATgGIAAgGQAAgoAiAAQAIABAMADIgFANQgJgDgHAAQgJAAgEAGQgEAGAAANIAAAHIAbAAIAAANIgbAAIAABdg");
	this.shape_5.setTransform(29.1,15.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgZAwQgLgGgGgNQgGgMAAgRQAAgZANgPQANgOAWAAQAXgBANAQQANAPAAAYQAAAagNAPQgNAOgXAAQgOABgLgIgAgXgeQgIAKAAAUQAAAUAIALQAIALAPAAQAQAAAIgLQAJgLAAgUQAAgUgJgKQgIgLgQAAQgPAAgIALg");
	this.shape_6.setTransform(19,18.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgsBDIAAgQQAJADAKADQALACAKAAQAPAAAJgGQAIgHAAgLQAAgIgDgEQgDgFgHgEQgHgEgNgFQgUgGgIgKQgJgJAAgQQAAgRAMgKQANgKATABQAVgBASAJIgFAOQgSgIgQABQgMgBgIAGQgHAGAAAKQAAAHACAFQADAFAHAEIATAIQAWAHAJAJQAIAKAAAPQAAASgOALQgNAKgXAAQgZAAgOgGg");
	this.shape_7.setTransform(7.5,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(0,0,86.8,31.3), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgnBGQgLgHABgPQAAgJAFgHQAHgIALgCQgEgCgCgEQgEgDAAgGQAAgGAEgDQACgFAIgEQgJgDgFgIQgGgJABgKQAAgSALgJQAKgJATAAQAHAAAIABIAkAAIAAAKIgTADIAEAJQACAFABAIQAAAPgLAKQgLAJgSAAIgJgBQgKAGAAAHQAAAFADACQAEABAJAAIASAAQASABAJAHQAJAHAAAOQAAASgPAJQgOAJgaAAQgVAAgMgIgAgcAhQgGAGAAAJQAAAJAHAEQAIAFAOAAQASAAAKgGQALgGgBgLQAAgIgFgEQgFgDgQAAIgSAAQgLAAgGAFgAgTg7QgHAFAAANQAAALAHAGQAGAFALAAQAYABAAgXQAAgZgYAAQgLAAgGAHg");
	this.shape.setTransform(95.8,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_1.setTransform(84.2,18);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIBJIAAhqIAQAAIAABqgAgGg1QgDgDAAgFQAAgGADgCQADgDADAAQAEAAACADQADACAAAGQAAAFgDADQgCACgEAAQgDAAgDgCg");
	this.shape_2.setTransform(75.5,16.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPAiIAAg/IgQAAIAAgHIAQgHIAGgXIAJAAIAAAZIAfAAIAAAMIgfAAIAAA/QAAAJAEAGQAFAEAIAAIAIAAIAHgCIAAANQgDABgFABIgJABQgeAAAAghg");
	this.shape_3.setTransform(69.4,16.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHBMIAAiXIAPAAIAACXg");
	this.shape_4.setTransform(63.4,15.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgiAtQgKgJAAgUIAAhGIARAAIAABFQAAANAGAHQAGAGAMAAQAQAAAIgJQAIgJAAgWIAAg3IAQAAIAABqIgOAAIgCgOIgBAAQgFAIgJAFQgIADgLAAQgTAAgKgJg");
	this.shape_5.setTransform(54.6,18.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AglAwIAAgOQAIAEAJACQAJACAIAAQAMAAAHgEQAGgEAAgIQAAgGgFgFQgFgEgPgGQgPgFgGgEQgHgEgDgGQgDgFAAgHQAAgNALgIQAKgHASAAQARgBARAIIgGAMQgQgGgNAAQgLAAgGAEQgFADAAAHQAAAEACADQACADAFADIASAHQATAHAGAHQAHAGAAAMQAAAOgLAIQgLAJgTgBQgWABgMgIg");
	this.shape_6.setTransform(43.8,18.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_7.setTransform(32.9,18);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgZAwQgLgGgGgNQgGgMAAgRQAAgZANgPQANgOAWAAQAXgBANAQQANAPAAAYQAAAagNAPQgNAOgXAAQgOABgLgIgAgXgeQgIAKAAAUQAAAUAIALQAIALAPAAQAQAAAIgLQAJgLAAgUQAAgUgJgKQgIgLgQAAQgPAAgIALg");
	this.shape_8.setTransform(20.6,18.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgkA2QgRgTAAgjQAAgVAIgRQAIgQARgJQAPgKAUABQAWAAARAIIgHAOQgQgIgQABQgXAAgNAPQgOAQAAAaQAAAcANAQQANAPAYAAQAPgBASgEIAAAOQgOAGgWgBQgeABgSgUg");
	this.shape_9.setTransform(8.6,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(0,0,103.5,31.3), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.arow();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(0,0,15,15), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,96,37), null);


(lib.bg_symbol = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg_symbol, new cjs.Rectangle(0,0,300,250), null);


// stage content:
(lib.GSC1879_ABMBannerAd_300x250Copy = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Your
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(-44.3,38.5,1,1,0,0,0,44.7,27.2);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({_off:false},0).wait(1).to({regX:44.2,regY:28.5,x:-16.9,y:39.7,alpha:0.25},0).wait(1).to({x:11,y:39.6,alpha:0.5},0).wait(1).to({x:38.9,y:39.4,alpha:0.75},0).wait(1).to({x:66.7,y:39.3,alpha:1},0).wait(121).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(24).to({regX:44.7,regY:27.2,x:-44.3,y:38.5},0).wait(1).to({regX:44.2,regY:28.5,x:-16.9,y:39.7,alpha:0.25},0).wait(1).to({x:11,y:39.6,alpha:0.5},0).wait(1).to({x:38.9,y:39.4,alpha:0.75},0).wait(1).to({x:66.7,y:39.3,alpha:1},0).wait(129));

	// Engineering 
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-100.7,72,1,1,0,0,0,116.2,27.2);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15).to({_off:false},0).wait(1).to({regX:112.7,regY:32,x:-44.1,y:77.4,alpha:0.25},0).wait(1).to({x:16,y:78,alpha:0.5},0).wait(1).to({x:76.1,y:78.5,alpha:0.75},0).wait(1).to({x:136.2,y:79.1,alpha:1},0).wait(116).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(5).to({_off:false,regX:116.2,regY:27.2,x:-100.7,y:72},0).wait(1).to({regX:112.7,regY:32,x:-44.1,y:77.4,alpha:0.25},0).wait(1).to({x:16,y:78,alpha:0.5},0).wait(1).to({x:76.1,y:78.5,alpha:0.75},0).wait(1).to({x:136.2,y:79.1,alpha:1},0).wait(124));

	// Co-Pilot
	this.instance_2 = new lib.Symbol8();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-73.2,113.7,1,1,0,0,0,73.7,27.2);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(20).to({_off:false},0).wait(1).to({regX:74,regY:27.7,x:-30.4,y:113.5,alpha:0.25},0).wait(1).to({x:12.1,y:112.7,alpha:0.5},0).wait(1).to({x:54.6,y:112,alpha:0.75},0).wait(1).to({x:97,y:111.2,alpha:1},0).wait(111).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(10).to({_off:false,regX:73.7,regY:27.2,x:-73.2,y:113.7},0).wait(1).to({regX:74,regY:27.7,x:-30.4,y:113.5,alpha:0.25},0).wait(1).to({x:12.1,y:112.7,alpha:0.5},0).wait(1).to({x:54.6,y:112,alpha:0.75},0).wait(1).to({x:97,y:111.2,alpha:1},0).wait(119));

	// arrow_01
	this.instance_3 = new lib.Symbol4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(7.5,158.4,1,1,0,0,0,7.5,7.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(35).to({_off:false},0).wait(1).to({x:15.5,y:158.5,alpha:0.333},0).wait(1).to({x:23.6,y:158.6,alpha:0.667},0).wait(1).to({x:31.6,y:158.7,alpha:1},0).wait(97).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(25).to({_off:false,x:7.5,y:158.4},0).wait(1).to({x:15.5,y:158.5,alpha:0.333},0).wait(1).to({x:23.6,y:158.6,alpha:0.667},0).wait(1).to({x:31.6,y:158.7,alpha:1},0).wait(105));

	// Consulting
	this.instance_4 = new lib.Symbol5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(104.3,158.2,1,1,0,0,0,51.7,15.6);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(35).to({_off:false},0).wait(1).to({regX:52,regY:18.2,x:96.6,y:160.8,alpha:1},0).wait(99).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(25).to({_off:false,regX:51.7,regY:15.6,x:104.3,y:158.2},0).wait(1).to({regX:52,regY:18.2,x:96.6,y:160.8,alpha:1},0).wait(107));

	// arrow_02
	this.instance_5 = new lib.Symbol4();
	this.instance_5.parent = this;
	this.instance_5.setTransform(7.5,187.6,1,1,0,0,0,7.5,7.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(45).to({_off:false},0).wait(1).to({x:15.9,alpha:0.333},0).wait(1).to({x:24.2,alpha:0.667},0).wait(1).to({x:32.6,alpha:1},0).wait(87).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(35).to({_off:false,x:7.5},0).wait(1).to({x:15.9,alpha:0.333},0).wait(1).to({x:24.2,alpha:0.667},0).wait(1).to({x:32.6,alpha:1},0).wait(95));

	// Software
	this.instance_6 = new lib.Symbol6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(98,187,1,1,0,0,0,43.4,15.6);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(45).to({_off:false},0).wait(1).to({regX:43.3,regY:15.9,x:87.9,y:187.8,alpha:1},0).wait(89).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(35).to({_off:false,regX:43.4,regY:15.6,x:98,y:187},0).wait(1).to({regX:43.3,regY:15.9,x:87.9,y:187.8,alpha:1},0).wait(97));

	// arrow_03
	this.instance_7 = new lib.Symbol4();
	this.instance_7.parent = this;
	this.instance_7.setTransform(7.5,217.8,1,1,0,0,0,7.5,7.5);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(55).to({_off:false},0).wait(1).to({x:16.1,y:217.7,alpha:0.333},0).wait(1).to({x:24.7,alpha:0.667},0).wait(1).to({x:33.4,y:217.6,alpha:1},0).wait(77).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(45).to({_off:false,x:7.5,y:217.8},0).wait(1).to({x:16.1,y:217.7,alpha:0.333},0).wait(1).to({x:24.7,alpha:0.667},0).wait(1).to({x:33.4,y:217.6,alpha:1},0).wait(85));

	// Training
	this.instance_8 = new lib.Symbol7();
	this.instance_8.parent = this;
	this.instance_8.setTransform(92.8,216.9,1,1,0,0,0,39.4,15.6);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(55).to({_off:false},0).wait(1).to({regX:39.1,regY:18.4,x:82.7,y:219.5,alpha:1},0).wait(79).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(45).to({_off:false,regX:39.4,regY:15.6,x:92.8,y:216.9},0).wait(1).to({regX:39.1,regY:18.4,x:82.7,y:219.5,alpha:1},0).wait(87));

	// logo
	this.instance_9 = new lib.Symbol1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(235,213.5,1,1,0,0,0,48,18.5);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(20).to({_off:false},0).wait(1).to({x:234.9,y:213.6,alpha:0.036},0).wait(1).to({x:234.8,y:213.7,alpha:0.071},0).wait(1).to({x:234.7,y:213.8,alpha:0.107},0).wait(1).to({x:234.5,y:213.9,alpha:0.143},0).wait(1).to({x:234.4,y:214,alpha:0.179},0).wait(1).to({x:234.3,y:214.1,alpha:0.214},0).wait(1).to({x:234.2,y:214.3,alpha:0.25},0).wait(1).to({x:234.1,y:214.4,alpha:0.286},0).wait(1).to({x:234,y:214.5,alpha:0.321},0).wait(1).to({x:233.8,y:214.6,alpha:0.357},0).wait(1).to({x:233.7,y:214.7,alpha:0.393},0).wait(1).to({x:233.6,y:214.8,alpha:0.429},0).wait(1).to({x:233.5,y:214.9,alpha:0.464},0).wait(1).to({x:233.4,y:215,alpha:0.5},0).wait(1).to({x:233.3,y:215.1,alpha:0.536},0).wait(1).to({x:233.1,y:215.2,alpha:0.571},0).wait(1).to({x:233,y:215.3,alpha:0.607},0).wait(1).to({x:232.9,y:215.4,alpha:0.643},0).wait(1).to({x:232.8,y:215.5,alpha:0.679},0).wait(1).to({x:232.7,y:215.6,alpha:0.714},0).wait(1).to({x:232.6,y:215.8,alpha:0.75},0).wait(1).to({x:232.4,y:215.9,alpha:0.786},0).wait(1).to({x:232.3,y:216,alpha:0.821},0).wait(1).to({x:232.2,y:216.1,alpha:0.857},0).wait(1).to({x:232.1,y:216.2,alpha:0.893},0).wait(1).to({x:232,y:216.3,alpha:0.929},0).wait(1).to({x:231.9,y:216.4,alpha:0.964},0).wait(1).to({x:231.8,y:216.5,alpha:1},0).wait(87).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(10).to({_off:false,x:235,y:213.5},0).wait(1).to({x:234.9,y:213.6,alpha:0.036},0).wait(1).to({x:234.8,y:213.7,alpha:0.071},0).wait(1).to({x:234.7,y:213.8,alpha:0.107},0).wait(1).to({x:234.5,y:213.9,alpha:0.143},0).wait(1).to({x:234.4,y:214,alpha:0.179},0).wait(1).to({x:234.3,y:214.1,alpha:0.214},0).wait(1).to({x:234.2,y:214.3,alpha:0.25},0).wait(1).to({x:234.1,y:214.4,alpha:0.286},0).wait(1).to({x:234,y:214.5,alpha:0.321},0).wait(1).to({x:233.8,y:214.6,alpha:0.357},0).wait(1).to({x:233.7,y:214.7,alpha:0.393},0).wait(1).to({x:233.6,y:214.8,alpha:0.429},0).wait(1).to({x:233.5,y:214.9,alpha:0.464},0).wait(1).to({x:233.4,y:215,alpha:0.5},0).wait(1).to({x:233.3,y:215.1,alpha:0.536},0).wait(1).to({x:233.1,y:215.2,alpha:0.571},0).wait(1).to({x:233,y:215.3,alpha:0.607},0).wait(1).to({x:232.9,y:215.4,alpha:0.643},0).wait(1).to({x:232.8,y:215.5,alpha:0.679},0).wait(1).to({x:232.7,y:215.6,alpha:0.714},0).wait(1).to({x:232.6,y:215.8,alpha:0.75},0).wait(1).to({x:232.4,y:215.9,alpha:0.786},0).wait(1).to({x:232.3,y:216,alpha:0.821},0).wait(1).to({x:232.2,y:216.1,alpha:0.857},0).wait(1).to({x:232.1,y:216.2,alpha:0.893},0).wait(1).to({x:232,y:216.3,alpha:0.929},0).wait(1).to({x:231.9,y:216.4,alpha:0.964},0).wait(1).to({x:231.8,y:216.5,alpha:1},0).wait(95));

	// bg
	this.instance_10 = new lib.bg_symbol();
	this.instance_10.parent = this;
	this.instance_10.setTransform(109.1,125.1,1.264,1.264,0,0,0.3,147.6,124);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1).to({regX:150,regY:125,x:112.4,y:126.4},0).wait(1).to({x:112.7},0).wait(1).to({x:112.9},0).wait(1).to({x:113.2},0).wait(1).to({x:113.4},0).wait(1).to({x:113.7},0).wait(1).to({x:114},0).wait(1).to({x:114.2},0).wait(1).to({x:114.5},0).wait(1).to({x:114.8},0).wait(1).to({x:115},0).wait(1).to({x:115.3},0).wait(1).to({x:115.5},0).wait(1).to({x:115.8},0).wait(1).to({x:116.1},0).wait(1).to({x:116.3},0).wait(1).to({x:116.6},0).wait(1).to({x:116.8},0).wait(1).to({x:117.1},0).wait(1).to({x:117.4},0).wait(1).to({x:117.6},0).wait(1).to({x:117.9},0).wait(1).to({x:118.1},0).wait(1).to({x:118.4},0).wait(1).to({x:118.7},0).wait(1).to({x:118.9},0).wait(1).to({x:119.2},0).wait(1).to({x:119.4},0).wait(1).to({x:119.7},0).wait(1).to({x:120},0).wait(1).to({x:120.2},0).wait(1).to({x:120.5},0).wait(1).to({x:120.8},0).wait(1).to({x:121},0).wait(1).to({x:121.3},0).wait(1).to({x:121.5},0).wait(1).to({x:121.8},0).wait(1).to({x:122.1},0).wait(1).to({x:122.3},0).wait(1).to({x:122.6},0).wait(1).to({x:122.8},0).wait(1).to({x:123.1},0).wait(1).to({x:123.4},0).wait(1).to({x:123.6},0).wait(1).to({x:123.9},0).wait(1).to({x:124.1},0).wait(1).to({x:124.4},0).wait(1).to({x:124.7},0).wait(1).to({x:124.9},0).wait(1).to({x:125.2},0).wait(1).to({x:125.4},0).wait(1).to({x:125.7},0).wait(1).to({x:126},0).wait(1).to({x:126.2},0).wait(1).to({x:126.5},0).wait(1).to({x:126.8},0).wait(1).to({x:127},0).wait(1).to({x:127.3},0).wait(1).to({x:127.5},0).wait(1).to({x:127.8},0).wait(1).to({x:128.1},0).wait(1).to({x:128.3},0).wait(1).to({x:128.6},0).wait(1).to({x:128.8},0).wait(1).to({x:129.1},0).wait(1).to({x:129.4},0).wait(1).to({x:129.6},0).wait(1).to({x:129.9},0).wait(1).to({x:130.1},0).wait(1).to({x:130.4},0).wait(1).to({x:130.7},0).wait(1).to({x:130.9},0).wait(1).to({x:131.2},0).wait(1).to({x:131.4},0).wait(1).to({x:131.7},0).wait(1).to({x:132},0).wait(1).to({x:132.2},0).wait(1).to({x:132.5},0).wait(1).to({x:132.8},0).wait(1).to({x:133},0).wait(1).to({x:133.3},0).wait(1).to({x:133.5},0).wait(1).to({x:133.8},0).wait(1).to({x:134.1},0).wait(1).to({x:134.3},0).wait(1).to({x:134.6},0).wait(1).to({x:134.8},0).wait(1).to({x:135.1},0).wait(1).to({x:135.4},0).wait(1).to({x:135.6},0).wait(1).to({x:135.9},0).wait(1).to({x:136.1},0).wait(1).to({x:136.4},0).wait(1).to({x:136.7},0).wait(1).to({x:136.9},0).wait(1).to({x:137.2},0).wait(1).to({x:137.4},0).wait(1).to({x:137.7},0).wait(1).to({x:138},0).wait(1).to({x:138.2},0).wait(1).to({x:138.5},0).wait(1).to({x:138.8},0).wait(1).to({x:139},0).wait(1).to({x:139.3},0).wait(1).to({x:139.5},0).wait(1).to({x:139.8},0).wait(1).to({x:140.1},0).wait(1).to({x:140.3},0).wait(1).to({x:140.6},0).wait(1).to({x:140.8},0).wait(1).to({x:141.1},0).wait(1).to({x:141.4},0).wait(1).to({x:141.6},0).wait(1).to({x:141.9},0).wait(1).to({x:142.1},0).wait(1).to({x:142.4},0).wait(1).to({x:142.7},0).wait(1).to({x:142.9},0).wait(1).to({x:143.2},0).wait(1).to({x:143.4},0).wait(1).to({x:143.7},0).wait(1).to({x:144},0).wait(1).to({x:144.2},0).wait(1).to({x:144.5},0).wait(1).to({x:144.8},0).wait(1).to({x:145},0).wait(1).to({x:145.3},0).wait(1).to({x:145.5},0).wait(1).to({x:145.8},0).wait(1).to({x:146.1},0).wait(1).to({x:146.3},0).wait(1).to({x:146.6},0).wait(1).to({x:146.8},0).wait(1).to({x:147.1},0).wait(1).to({x:147.4},0).wait(1).to({x:147.6},0).wait(1).to({x:147.9},0).wait(1).to({x:148.1},0).wait(1).to({x:148.4},0).wait(1).to({x:148.7},0).wait(1).to({x:148.9},0).wait(1).to({x:149.2},0).wait(1).to({x:149.4},0).wait(1).to({x:149.7},0).wait(1).to({x:150},0).wait(1).to({x:150.2},0).wait(1).to({x:150.5},0).wait(1).to({x:150.8},0).wait(1).to({x:151},0).wait(1).to({x:151.3},0).wait(1).to({x:151.5},0).wait(1).to({x:151.8},0).wait(1).to({x:152.1},0).wait(1).to({x:152.3},0).wait(1).to({x:152.6},0).wait(1).to({x:152.8},0).wait(1).to({x:153.1},0).wait(1).to({x:153.4},0).wait(1).to({x:153.6},0).wait(1).to({x:153.9},0).wait(1).to({x:154.1},0).wait(1).to({x:154.4},0).wait(1).to({x:154.7},0).wait(1).to({x:154.9},0).wait(1).to({x:155.2},0).wait(1).to({x:155.4},0).wait(1).to({x:155.7},0).wait(1).to({x:156},0).wait(1).to({x:156.2},0).wait(1).to({x:156.5},0).wait(1).to({x:156.8},0).wait(1).to({x:157},0).wait(1).to({x:157.3},0).wait(1).to({x:157.5},0).wait(1).to({x:157.8},0).wait(1).to({x:158.1},0).wait(1).to({x:158.3},0).wait(1).to({x:158.6},0).wait(1).to({x:158.8},0).wait(1).to({x:159.1},0).wait(1).to({x:159.4},0).wait(1).to({x:159.6},0).wait(1).to({x:159.9},0).wait(1).to({x:160.1},0).wait(1).to({x:160.4},0).wait(1).to({x:160.7},0).wait(1).to({x:160.9},0).wait(1).to({x:161.2},0).wait(1).to({x:161.4},0).wait(1).to({x:161.7},0).wait(1).to({x:162},0).wait(1).to({x:162.2},0).wait(1).to({x:162.5},0).wait(1).to({x:162.8},0).wait(1).to({x:163},0).wait(1).to({x:163.3},0).wait(1).to({x:163.5},0).wait(1).to({x:163.8},0).wait(1).to({x:164.1},0).wait(1).to({x:164.3},0).wait(1).to({x:164.6},0).wait(1).to({x:164.8},0).wait(1).to({x:165.1},0).wait(1).to({x:165.4},0).wait(1).to({x:165.6},0).wait(1).to({x:165.9},0).wait(1).to({x:166.1},0).wait(1).to({x:166.4},0).wait(1).to({x:166.7},0).wait(1).to({x:166.9},0).wait(1).to({x:167.2},0).wait(1).to({x:167.4},0).wait(1).to({x:167.7},0).wait(1).to({x:168},0).wait(1).to({x:168.2},0).wait(1).to({x:168.5},0).wait(1).to({x:168.8},0).wait(1).to({x:169},0).wait(1).to({x:169.3},0).wait(1).to({x:169.5},0).wait(1).to({x:169.8},0).wait(1).to({x:170.1},0).wait(1).to({x:170.3},0).wait(1).to({x:170.6},0).wait(1).to({x:170.8},0).wait(1).to({x:171.1},0).wait(1).to({x:171.4},0).wait(1).to({x:171.6},0).wait(1).to({x:171.9},0).wait(1).to({x:172.1},0).wait(1).to({x:172.4},0).wait(1).to({x:172.7},0).wait(1).to({x:172.9},0).wait(1).to({x:173.2},0).wait(1).to({x:173.4},0).wait(1).to({x:173.7},0).wait(1).to({x:174},0).wait(1).to({x:174.2},0).wait(1).to({x:174.5},0).wait(1).to({x:174.8},0).wait(1).to({x:175},0).wait(1).to({x:175.3},0).wait(1).to({x:175.5},0).wait(1).to({x:175.8},0).wait(1).to({x:176.1},0).wait(1).to({x:176.3},0).wait(1).to({x:176.6},0).wait(1).to({x:176.8},0).wait(1).to({x:177.1},0).wait(1).to({x:177.4},0).wait(1).to({x:177.6},0).wait(1).to({x:177.9},0).wait(1).to({x:178.1},0).wait(1).to({x:178.4},0).wait(1).to({x:178.7},0).wait(1).to({x:178.9},0).wait(1).to({x:179.2},0).wait(1).to({x:179.4},0).wait(1).to({x:179.7},0).wait(1).to({x:180},0).wait(1).to({x:180.2},0).wait(1).to({x:180.5},0).wait(1).to({x:180.8},0).wait(1).to({x:181},0).wait(1).to({x:181.3},0).wait(1).to({x:181.5},0).wait(1).to({x:181.8},0).wait(1).to({x:182.1},0).wait(1).to({x:182.3},0).wait(1).to({x:182.6},0).wait(1).to({x:182.8},0).wait(1).to({x:183.1},0).wait(1).to({x:183.4},0).wait(1).to({x:183.6},0).wait(1).to({x:183.9},0).wait(1).to({x:184.1},0).wait(1).to({x:184.4},0).wait(1).to({x:184.7},0).wait(1).to({x:184.9},0).wait(1).to({x:185.2},0).wait(1).to({x:185.4},0).wait(1).to({x:185.7},0).wait(1).to({x:186},0).wait(1).to({x:186.2},0).wait(1).to({x:186.5},0).wait(1).to({x:186.8},0).wait(1).to({x:187},0).wait(1).to({x:187.3},0).wait(1).to({x:187.5},0).wait(1).to({x:187.8},0).wait(1).to({x:188.1},0).wait(1).to({x:188.3},0).wait(1).to({x:188.6},0).wait(1).to({x:188.8},0).wait(1).to({x:189.1},0).wait(1).to({x:189.4},0).wait(1).to({x:189.6},0).wait(1).to({x:189.8},0).wait(1).to({x:190.1},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(72.4,92.4,379.2,317.8);
// library properties:
lib.properties = {
	id: 'D4C83844DFB7CD4CA2819D590FAC090D',
	width: 300,
	height: 250,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/GSC1879_ABM_Banner_Ad_300x250_atlas_.png", id:"GSC1879_ABM_Banner_Ad_300x250_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D4C83844DFB7CD4CA2819D590FAC090D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;