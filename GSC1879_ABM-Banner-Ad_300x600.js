(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"GSC1879_ABM_Banner_Ad_300x600_atlas_", frames: [[0,0,300,600],[0,602,175,67]]}
];


// symbols:



(lib.GSC1879_ABMBannerAd_300x600_bg = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x600_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logo_big = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x600_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Your = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhNB6IAAjvIAxAAIAKAoIADAAQALgUATgMQASgMAXAAQAOAAAIABIgFA+QgIgCgLAAQgfAAgSAQQgQAQAAAcIAAB6g");
	this.shape.setTransform(94.7,36.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhZBjQgVgWAAgrIAAibIBBAAIAACLQABAaAJANQAJANAUAAQAbAAAMgSQAMgTAAgpIAAhxIBCAAIAADvIgyAAIgJgfIgEAAQgKARgUAJQgSAJgZAAQgqAAgWgXg");
	this.shape_1.setTransform(68.9,37.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag9BtQgagPgOgdQgOgcAAglQAAg6AeghQAfghA3AAQAiAAAbAQQAaAPAOAcQAOAbAAAmQAAA6gfAhQgeAhg3ABQgigBgbgPgAglg1QgMARAAAkQAAAjAMASQAMASAZAAQAaAAAMgSQALgRAAgkQAAgjgLgSQgMgSgaAAQgZAAgMASg");
	this.shape_2.setTransform(40.9,37.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AggCdIAAh4IhojBIBIAAIBACBIBCiBIBHAAIhoC/IAAB6g");
	this.shape_3.setTransform(15.7,33.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Your, new cjs.Rectangle(0,0,105.6,64), null);


(lib.Training = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhFB/QgVgOAAgaQAAgRALgNQAMgNAUgEQgHgEgFgHQgFgHAAgJQAAgLAFgHQAGgHAMgIQgPgGgJgPQgKgPAAgTQAAgfATgRQATgRAjAAQAOAAAMADIBDAAIAAATIgkAEQAFAGAEAKQAEAKAAAMQAAAdgTAQQgUARghAAQgIAAgIgBQgSAKAAAOQAAAIAGADQAHAEAPAAIAiAAQAfAAARANQAQAOAAAZQAAAggZARQgaAQgwAAQgmAAgUgOgAgzA8QgLAJAAASQAAAQANAIQAOAIAYAAQAkAAASgLQARgLAAgTQAAgQgJgFQgKgHgbAAIgiAAQgUAAgLAKgAgjhsQgLALAAAWQAAAUALAKQAMALAUAAQAqAAAAgqQAAgrgqAAQgVAAgLALg");
	this.shape.setTransform(126.2,35);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAzBiIAAh7QABgYgLgMQgLgMgXAAQgeABgOAQQgNAQAAAmIAABkIgeAAIAAjAIAYAAIAFAbIACAAQAIgPAQgHQARgIASAAQAjAAASARQARARAAAlIAAB8g");
	this.shape_1.setTransform(105.6,30.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOCEIAAjAIAcAAIAADAgAgLhhQgFgFAAgKQAAgKAFgEQAFgFAGAAQAHAAAFAFQAFAEAAAKQAAAKgFAFQgFAFgHgBQgGABgFgFg");
	this.shape_2.setTransform(89.9,27.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AA0BiIAAh7QgBgYgKgMQgLgMgXAAQgeABgNAQQgPAQAAAmIAABkIgdAAIAAjAIAYAAIAFAbIACAAQAIgPAQgHQARgIASAAQAjAAASARQARARAAAlIAAB8g");
	this.shape_3.setTransform(74.4,30.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgOCEIAAjAIAcAAIAADAgAgLhhQgFgFAAgKQAAgKAFgEQAFgFAGAAQAHAAAFAFQAFAEAAAKQAAAKgFAFQgFAFgHgBQgGABgFgFg");
	this.shape_4.setTransform(58.7,27.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag8BVQgQgPAAgbQAAg5BcgDIAhgBIAAgMQAAgXgKgLQgKgKgVAAQgXAAgfAOIgJgWQAPgIARgEQARgFAQAAQAiAAARAQQAQAPAAAiIAACCIgVAAIgGgbIgCAAQgOASgOAGQgPAHgUAAQgdAAgQgPgAATADQgiABgPAKQgQAJAAAUQAAAQAKAIQAKAJARAAQAaAAAQgPQAPgPAAgbIAAgRg");
	this.shape_5.setTransform(43.5,30.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ag3BiIAAjAIAYAAIAEAkIACAAQAKgTAPgKQAOgKASAAQANAAALACIgFAbQgMgCgJAAQgXgBgQAUQgQASAAAcIAABng");
	this.shape_6.setTransform(28.8,30.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgOCBIAAjmIhRAAIAAgbIC/AAIAAAbIhRAAIAADmg");
	this.shape_7.setTransform(12.1,27.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Training, new cjs.Rectangle(0,0,138.2,53.1), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo_big();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(0,0,175,67), null);


(lib.Software = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag4BKQgZgaAAguQAAguAYgbQAWgcAnAAQAjAAAVAYQAWAYgBAnIAAARIiDAAQABAiAQASQAQARAdAAQAfAAAegNIAAAaQgPAHgOADQgOADgTAAQgqAAgZgagAghg7QgOAPgCAaIBkAAQAAgbgNgPQgMgOgXAAQgXAAgNAPg");
	this.shape.setTransform(140.7,30.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag2BiIAAjAIAYAAIADAkIABAAQALgTAPgKQAOgKATAAQAMAAAKACIgDAbQgMgCgKAAQgXgBgQAUQgQASgBAcIAABng");
	this.shape_1.setTransform(125.2,30.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("Ag8BVQgQgPAAgbQAAg5BcgDIAhgBIAAgMQAAgXgKgLQgKgKgVAAQgXAAgfAOIgJgWQAPgIARgEQARgFAQAAQAiAAARAQQAQAPAAAiIAACCIgVAAIgGgbIgCAAQgOASgOAGQgPAHgUAAQgdAAgQgPgAATADQgiABgPAKQgQAJAAAUQAAAQAKAIQAKAJARAAQAaAAAQgPQAPgPAAgbIAAgRg");
	this.shape_2.setTransform(105.9,30.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAxBgIgkhwIgNgvIAAAAQgHAfgFARIgkBvIgiAAIg1i/IAeAAIAdBtQAKAmABANIABAAIAHgaQAEgQAEgJIAihtIAgAAIAiBtQAKAeADAVIABAAIAFgUIAkiMIAeAAIg1C/g");
	this.shape_3.setTransform(82.5,30.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgcA9IAAhxIgcAAIAAgOIAcgMIAMgqIAQAAIAAAtIA3AAIAAAXIg3AAIAABwQAAASAIAJQAIAJAOAAIAPgBIAMgCIAAAWQgFACgJACIgQACQg3AAAAg8g");
	this.shape_4.setTransform(62.1,28.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgfCKIAAipIgiAAIAAgNIAigLIAAgLQAAhHA9AAQAQAAAUAGIgHAYQgRgGgMAAQgRAAgIALQgHALAAAZIAAAMIAxAAIAAAXIgxAAIAACpg");
	this.shape_5.setTransform(50.7,26.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgtBYQgVgMgLgXQgLgXABgeQgBguAZgbQAXgaApAAQAoAAAYAbQAXAbABAtQgBAvgXAaQgXAbgqAAQgaAAgTgMgAgqg3QgPASAAAlQAAAlAPATQAOATAcAAQAdAAAPgTQAOgTAAglQAAgkgOgTQgPgTgdAAQgcAAgOATg");
	this.shape_6.setTransform(32.7,30.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhQB4IAAgcQAQAGASAEQATAEASAAQAeAAAOgMQAPgLABgUQAAgOgGgIQgFgJgNgGQgMgIgZgJQgkgMgPgSQgPgRAAgdQgBgdAXgSQAWgRAkAAQAmAAAhAOIgKAaQgggOgeABQgXgBgNAKQgOALAAASQAAAOAGAIQAFAJAMAGQALAIAXAIQApANAPARQAPARAAAaQAAAigZATQgZATgpAAQgtAAgZgMg");
	this.shape_7.setTransform(12.1,27.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Software, new cjs.Rectangle(0,0,152.8,53.1), null);


(lib.Engineering = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhdCeQgbgSAAggQAAgVANgPQAOgPAagGQgKgEgHgKQgIgJAAgMQAAgNAIgIQAIgKAPgIQgTgJgLgRQgLgTAAgYQAAgnAZgWQAagVAwAAIAWABIASADIBUAAIAAAiIgmAJQAKARAAATQAAAngaAWQgcAVguAAIgMgBIgJAAQgLAHAAAJQAAAOAlAAIAnAAQAnAAAWARQAUARAAAhQAAAqgjAXQgjAXhBAAQgyAAgagRgAgzBRQgMAIAAAPQgBANANAHQANAJAYgBQAiAAAUgJQAUgKAAgRQAAgNgMgFQgLgFgZgBIghAAQgSAAgMAJgAgnhbQAAATAJAMQAIAMATAAQARAAAJgMQAIgMAAgTQAAgsgiAAQgkAAAAAsg");
	this.shape.setTransform(249.4,42.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAtB6IAAiLQAAgagJgNQgJgNgUAAQgbAAgMASQgMATAAApIAABxIhCAAIAAjvIAyAAIAJAfIAEAAQALgSATgIQASgJAZAAQApAAAWAWQAWAXAAArIAACbg");
	this.shape_1.setTransform(222.7,36.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AggCnIAAjvIBBAAIAADvgAgjiGQAAggAjAAQAkAAAAAgQAAAPgJAJQgJAIgSAAQgjAAAAggg");
	this.shape_2.setTransform(201.5,32.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AhNB6IAAjvIAxAAIAKAoIADAAQALgUATgMQASgMAXAAQAOAAAIABIgFA+QgIgCgLAAQgfAAgSAQQgQAQAAAcIAAB6g");
	this.shape_3.setTransform(186,36.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhNBcQgggfgBg8QAAg7AeghQAfggA0gBQAzAAAcAeQAdAdAAAyIAAAfIibAAQACAcAPAQQAQAPAaABQAXAAATgFQATgFAVgKIAAAzQgRAJgUAEQgTAEgcABQg5AAghghgAgdhAQgLANgCAYIBbAAQgBgYgMgNQgMgNgUAAQgVAAgMANg");
	this.shape_4.setTransform(161.8,37.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AhNBcQgggfgBg8QAAg7AeghQAfggA0gBQAzAAAcAeQAdAdAAAyIAAAfIibAAQACAcAPAQQAPAPAbABQAWAAAUgFQATgFAVgKIAAAzQgRAJgUAEQgTAEgcABQg6AAggghgAgdhAQgLANgCAYIBbAAQgBgYgMgNQgMgNgUAAQgVAAgMANg");
	this.shape_5.setTransform(135.8,37.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAtB6IAAiLQAAgagJgNQgJgNgUAAQgbAAgMASQgMATAAApIAABxIhCAAIAAjvIAyAAIAJAfIAEAAQALgSATgIQASgJAZAAQApAAAWAWQAWAXAAArIAACbg");
	this.shape_6.setTransform(108.4,36.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AggCnIAAjvIBBAAIAADvgAgjiGQAAggAjAAQAkAAAAAgQAAAPgJAJQgJAIgSAAQgjAAAAggg");
	this.shape_7.setTransform(87.1,32.3);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AheCeQgagSAAggQAAgVAOgPQANgPAagGQgKgEgIgKQgHgJAAgMQAAgNAIgIQAIgKAPgIQgTgJgLgRQgLgTAAgYQAAgnAZgWQAagVAvAAIAXABIASADIBUAAIAAAiIgmAJQALARgBATQABAngbAWQgbAVgvAAIgMgBIgJAAQgLAHAAAJQAAAOAkAAIAoAAQAnAAAWARQAUARAAAhQAAAqgjAXQgjAXhBAAQgyAAgbgRgAgzBRQgMAIAAAPQAAANAMAHQANAJAYgBQAiAAAUgJQAUgKAAgRQAAgNgMgFQgMgFgYgBIghAAQgSAAgMAJgAgohbQABATAIAMQAJAMASAAQASAAAIgMQAJgMAAgTQAAgsgjAAQgjAAgBAsg");
	this.shape_8.setTransform(67.8,42.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAtB6IAAiLQAAgagJgNQgJgNgUAAQgbAAgMASQgMATAAApIAABxIhCAAIAAjvIAyAAIAJAfIAEAAQALgSATgIQASgJAZAAQApAAAWAWQAWAXAAArIAACbg");
	this.shape_9.setTransform(41.2,36.8);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhaCdIAAk5IC0AAIAAA3IhxAAIAABEIBpAAIAAA2IhpAAIAABRIBxAAIAAA3g");
	this.shape_10.setTransform(15,33.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Engineering, new cjs.Rectangle(0,0,264.2,64), null);


(lib.Consulting = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhFB/QgVgOAAgaQAAgRALgNQAMgNAUgEQgHgEgFgHQgFgHAAgJQAAgLAFgHQAGgHAMgIQgPgGgJgPQgKgPAAgTQAAgfATgRQATgRAjAAQAOAAAMADIBDAAIAAATIgkAEQAFAGAEAKQAEAKAAAMQAAAdgTAQQgUARghAAQgIAAgIgBQgSAKAAAOQAAAIAGADQAHAEAPAAIAiAAQAfAAARANQAQAOAAAZQAAAggZARQgaAQgwAAQgmAAgUgOgAgzA8QgLAJAAASQAAAQANAIQAOAIAYAAQAkAAASgLQARgLAAgTQAAgQgJgFQgKgHgbAAIgiAAQgUAAgLAKgAgjhsQgLALAAAWQAAAUALAKQAMALAUAAQAqAAAAgqQAAgrgqAAQgVAAgLALg");
	this.shape.setTransform(170.5,35);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAzBiIAAh7QABgYgMgMQgKgMgXAAQgdABgPAQQgOAQABAmIAABkIgdAAIAAjAIAXAAIAFAbIACAAQAIgPARgHQAPgIAUAAQAiAAASARQARARAAAlIAAB8g");
	this.shape_1.setTransform(149.9,30.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgOCEIAAjAIAcAAIAADAgAgLhhQgFgFAAgKQAAgKAFgEQAFgFAGAAQAHAAAFAFQAFAEAAAKQAAAKgFAFQgFAFgHgBQgGABgFgFg");
	this.shape_2.setTransform(134.2,27.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgcA9IAAhxIgcAAIAAgOIAcgMIAMgqIAQAAIAAAtIA3AAIAAAXIg3AAIAABwQAAASAIAJQAJAJANAAIAQgBIALgCIAAAWQgFACgJACIgQACQg3AAAAg8g");
	this.shape_3.setTransform(123.2,28.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgOCJIAAkRIAcAAIAAERg");
	this.shape_4.setTransform(112.4,26.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag+BSQgRgSAAgkIAAh9IAdAAIAAB8QAAAYAKALQALAMAXAAQAdAAAOgRQAPgRAAglIAAhkIAdAAIAADAIgZAAIgEgbIgBAAQgKAPgPAIQgQAHgTAAQgjAAgSgQg");
	this.shape_5.setTransform(96.7,31.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AhDBYIAAgbQAOAHAQAEQAQAEAPAAQAWAAAMgIQAMgHAAgPQAAgLgJgIQgKgIgbgLQgbgJgMgHQgLgIgFgJQgGgKAAgNQAAgYATgNQATgOAhAAQAfAAAdANIgKAXQgdgLgYAAQgTAAgLAGQgKAHAAALQAAAIAEAFQAEAGAIAFIAhAOQAiAMANAMQALANAAATQAAAbgUAOQgUAPgjAAQgmAAgWgMg");
	this.shape_6.setTransform(77.3,30.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAzBiIAAh7QAAgYgLgMQgKgMgXAAQgdABgPAQQgOAQABAmIAABkIgdAAIAAjAIAXAAIAFAbIABAAQAKgPAQgHQAPgIAUAAQAiAAASARQARARABAlIAAB8g");
	this.shape_7.setTransform(57.7,30.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AguBYQgTgMgMgXQgLgXABgeQAAguAXgbQAYgaApAAQAoAAAYAbQAXAbAAAtQAAAvgXAaQgXAbgqAAQgZAAgVgMgAgqg3QgPASAAAlQAAAlAPATQAPATAbAAQAdAAAOgTQAQgTgBglQABgkgQgTQgOgTgdAAQgcAAgOATg");
	this.shape_8.setTransform(35.7,30.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AhBBiQgfgjAAg/QAAgnAPgeQAOgeAdgQQAcgQAkAAQApAAAeAPIgNAZQgdgOgdABQgpAAgZAcQgYAcAAAwQAAAzAXAbQAYAbArAAQAbABAigKIAAAaQgbAKgnAAQg4AAgegig");
	this.shape_9.setTransform(14,27.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Consulting, new cjs.Rectangle(0,0,182.5,53.1), null);


(lib.CoPilot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AghCAQgSgUAAgnIAAhzIgfAAIAAgcIAkgWIATgzIApAAIAAAzIBCAAIAAAyIhCAAIAABzQAAAOAIAGQAHAHANAAQASAAAXgIIAAAxQgYALgkAAQgmAAgSgUg");
	this.shape.setTransform(162.6,34.7);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("Ag9BtQgagPgOgdQgOgcAAglQAAg6AeghQAfghA3AAQAiAAAbAQQAaAPAOAcQAOAbAAAmQAAA6gfAhQgeAhg3ABQgigBgbgPgAglg1QgMARAAAkQAAAjAMASQAMASAZAAQAaAAAMgSQALgRAAgkQAAgjgLgSQgMgSgaAAQgZAAgMASg");
	this.shape_1.setTransform(139.6,37.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AggCnIAAlNIBBAAIAAFNg");
	this.shape_2.setTransform(119.3,32.3);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AggCnIAAjvIBBAAIAADvgAgjiGQAAggAjAAQAkAAAAAgQAAAPgJAJQgJAIgSAAQgjAAAAggg");
	this.shape_3.setTransform(105.8,32.3);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhrCdIAAk5IBkAAQA4AAAeAYQAdAZABAxQgBAzgfAZQggAbg6AAIgcAAIAABwgAgpgJIAWAAQAeABAPgMQAPgMAAgYQAAgXgNgLQgMgLgaAAIgfAAg");
	this.shape_4.setTransform(86.3,33.4);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag5AbIAAg1IBzAAIAAA1g");
	this.shape_5.setTransform(64.4,37.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("Ag9BtQgagPgOgdQgOgcAAglQAAg6AeghQAfghA3AAQAiAAAbAQQAaAPAOAcQAOAbAAAmQAAA6gfAhQgeAhg3ABQgigBgbgPgAglg1QgMARAAAkQAAAjAMASQAMASAZAAQAaAAAMgSQALgRAAgkQAAgjgLgSQgMgSgaAAQgZAAgMASg");
	this.shape_6.setTransform(43.7,37.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhSB4QglgqAAhOQAAgwASgkQASgmAhgTQAigUAsAAQAtAAAvAXIgWA2QgRgIgSgHQgSgGgRAAQglAAgVAcQgUAcAAAyQAABpBOAAQAhAAAvgRIAAA4QgnARgwAAQhDAAgkgqg");
	this.shape_7.setTransform(16.5,33.4);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.CoPilot, new cjs.Rectangle(0,0,174.6,64), null);


(lib.arrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#53BCEB").s().p("AgRB6IBKh5IhKh7IBPAAIBFB8IhCB4gAiCB6IBLh5IhLh7IBPAAIBEB8IhAB4g");
	this.shape.setTransform(13.1,12.3);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.arrow, new cjs.Rectangle(0,0,26.2,24.5), null);


// stage content:
(lib.GSC1879_ABMBannerAd_300x600 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Training
	this.instance = new lib.Training();
	this.instance.parent = this;
	this.instance.setTransform(139.1,448.5,1,1,0,0,0,69.1,26.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(37).to({_off:false},0).wait(1).to({regX:68.9,regY:31.6,x:135.3,y:453.6,alpha:0.25},0).wait(1).to({x:131.7,alpha:0.5},0).wait(1).to({x:128,alpha:0.75},0).wait(1).to({x:124.4,alpha:1},0).wait(48).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(37).to({_off:false,regX:69.1,regY:26.5,x:139.1,y:448.5},0).wait(1).to({regX:68.9,regY:31.6,x:135.3,y:453.6,alpha:0.25},0).wait(1).to({x:131.7,alpha:0.5},0).wait(1).to({x:128,alpha:0.75},0).wait(1).to({x:124.4,alpha:1},0).wait(21));

	// Arrow 3
	this.instance_1 = new lib.arrow();
	this.instance_1.parent = this;
	this.instance_1.setTransform(11.6,450.5,1,1,0,0,0,13.2,12.6);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(37).to({_off:false},0).wait(1).to({regX:13.1,regY:12.3,x:17.8,y:450.2,alpha:0.25},0).wait(1).to({x:24,alpha:0.5},0).wait(1).to({x:30.3,alpha:0.75},0).wait(1).to({x:36.5,alpha:1},0).wait(48).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(37).to({_off:false,regX:13.2,regY:12.6,x:11.6,y:450.5},0).wait(1).to({regX:13.1,regY:12.3,x:17.8,y:450.2,alpha:0.25},0).wait(1).to({x:24,alpha:0.5},0).wait(1).to({x:30.3,alpha:0.75},0).wait(1).to({x:36.5,alpha:1},0).wait(21));

	// Software
	this.instance_2 = new lib.Software();
	this.instance_2.parent = this;
	this.instance_2.setTransform(147.4,395.4,1,1,0,0,0,76.4,26.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(33).to({_off:false},0).wait(1).to({regY:26.9,x:143.9,y:395.8,alpha:0.25},0).wait(1).to({x:140.4,alpha:0.5},0).wait(1).to({x:136.9,alpha:0.75},0).wait(1).to({x:133.4,alpha:1},0).wait(52).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(33).to({_off:false,regY:26.5,x:147.4,y:395.4},0).wait(1).to({regY:26.9,x:143.9,y:395.8,alpha:0.25},0).wait(1).to({x:140.4,alpha:0.5},0).wait(1).to({x:136.9,alpha:0.75},0).wait(1).to({x:133.4,alpha:1},0).wait(25));

	// Arrow 2
	this.instance_3 = new lib.arrow();
	this.instance_3.parent = this;
	this.instance_3.setTransform(11.1,398,1,1,0,0,0,13.2,12.6);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(33).to({_off:false},0).wait(1).to({regX:13.1,regY:12.3,x:17.5,y:397.7,alpha:0.25},0).wait(1).to({x:24,alpha:0.5},0).wait(1).to({x:30.5,alpha:0.75},0).wait(1).to({x:37,alpha:1},0).wait(52).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(33).to({_off:false,regX:13.2,regY:12.6,x:11.1,y:398},0).wait(1).to({regX:13.1,regY:12.3,x:17.5,y:397.7,alpha:0.25},0).wait(1).to({x:24,alpha:0.5},0).wait(1).to({x:30.5,alpha:0.75},0).wait(1).to({x:37,alpha:1},0).wait(25));

	// Consulting
	this.instance_4 = new lib.Consulting();
	this.instance_4.parent = this;
	this.instance_4.setTransform(161.3,339,1,1,0,0,0,91.3,26.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(29).to({_off:false},0).wait(1).to({regX:92,regY:31.1,x:158.8,y:343.6,alpha:0.25},0).wait(1).to({x:155.5,alpha:0.5},0).wait(1).to({x:152.3,alpha:0.75},0).wait(1).to({x:149,alpha:1},0).wait(56).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(29).to({_off:false,regX:91.3,regY:26.5,x:161.3,y:339},0).wait(1).to({regX:92,regY:31.1,x:158.8,y:343.6,alpha:0.25},0).wait(1).to({x:155.5,alpha:0.5},0).wait(1).to({x:152.3,alpha:0.75},0).wait(1).to({x:149,alpha:1},0).wait(29));

	// Arrow 1
	this.instance_5 = new lib.arrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(11.8,341.6,1,1,0,0,0,13.2,12.6);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(29).to({_off:false},0).wait(1).to({regX:13.1,regY:12.3,x:17.9,y:341.3,alpha:0.25},0).wait(1).to({x:24.2,alpha:0.5},0).wait(1).to({x:30.4,alpha:0.75},0).wait(1).to({x:36.7,alpha:1},0).wait(56).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(29).to({_off:false,regX:13.2,regY:12.6,x:11.8,y:341.6},0).wait(1).to({regX:13.1,regY:12.3,x:17.9,y:341.3,alpha:0.25},0).wait(1).to({x:24.2,alpha:0.5},0).wait(1).to({x:30.4,alpha:0.75},0).wait(1).to({x:36.7,alpha:1},0).wait(29));

	// Co-Pilot
	this.instance_6 = new lib.CoPilot();
	this.instance_6.parent = this;
	this.instance_6.setTransform(149.8,274.9,1,1,0,0,0,87.3,31.9);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(17).to({_off:false},0).wait(1).to({regX:87.8,regY:32.5,x:139.1,y:275.5,alpha:0.25},0).wait(1).to({x:127.8,alpha:0.5},0).wait(1).to({x:116.6,alpha:0.75},0).wait(1).to({x:105.3,alpha:1},0).wait(68).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(17).to({_off:false,regX:87.3,regY:31.9,x:149.8,y:274.9},0).wait(1).to({regX:87.8,regY:32.5,x:139.1,y:275.5,alpha:0.25},0).wait(1).to({x:127.8,alpha:0.5},0).wait(1).to({x:116.6,alpha:0.75},0).wait(1).to({x:105.3,alpha:1},0).wait(41));

	// Engineering
	this.instance_7 = new lib.Engineering();
	this.instance_7.parent = this;
	this.instance_7.setTransform(179.1,225.9,1,1,0,0,0,132.1,31.9);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(13).to({_off:false},0).wait(1).to({regX:133.7,regY:37.6,x:173.1,y:231.6,alpha:0.25},0).wait(1).to({x:165.5,alpha:0.5},0).wait(1).to({x:157.8,alpha:0.75},0).wait(1).to({x:150.2,alpha:1},0).wait(72).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(13).to({_off:false,regX:132.1,regY:31.9,x:179.1,y:225.9},0).wait(1).to({regX:133.7,regY:37.6,x:173.1,y:231.6,alpha:0.25},0).wait(1).to({x:165.5,alpha:0.5},0).wait(1).to({x:157.8,alpha:0.75},0).wait(1).to({x:150.2,alpha:1},0).wait(45));

	// Your
	this.instance_8 = new lib.Your();
	this.instance_8.parent = this;
	this.instance_8.setTransform(101.3,178.3,1,1,0,0,0,52.8,31.9);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(9).to({_off:false},0).wait(1).to({regX:52.2,regY:33.6,x:92.8,y:180,alpha:0.25},0).wait(1).to({x:85,alpha:0.5},0).wait(1).to({x:77.1,alpha:0.75},0).wait(1).to({x:69.2,alpha:1},0).wait(76).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(9).to({_off:false,regX:52.8,regY:31.9,x:101.3,y:178.3},0).wait(1).to({regX:52.2,regY:33.6,x:92.8,y:180,alpha:0.25},0).wait(1).to({x:85,alpha:0.5},0).wait(1).to({x:77.1,alpha:0.75},0).wait(1).to({x:69.2,alpha:1},0).wait(49));

	// logo
	this.instance_9 = new lib.Symbol1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(109.5,85.5,1,1,0,0,0,87.5,33.5);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(85).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).wait(2).to({alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(58));

	// Backgound
	this.instance_10 = new lib.GSC1879_ABMBannerAd_300x600_bg();
	this.instance_10.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(156));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(150,300,300,600);
// library properties:
lib.properties = {
	id: 'D4C83844DFB7CD4CA2819D590FAC090D',
	width: 300,
	height: 600,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/GSC1879_ABM_Banner_Ad_300x600_atlas_.png", id:"GSC1879_ABM_Banner_Ad_300x600_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D4C83844DFB7CD4CA2819D590FAC090D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;