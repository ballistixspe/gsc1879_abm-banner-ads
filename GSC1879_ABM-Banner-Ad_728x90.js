(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"GSC1879_ABM_Banner_Ad_728x90_atlas_", frames: [[0,0,728,90],[730,0,175,67]]}
];


// symbols:



(lib.GSC1879_ABMBannerAd_728x90_1 = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_728x90_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logo_big = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_728x90_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Your = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("Ag/BkIAAjDIAoAAIAIAgIAEAAQAIgQAPgKQAPgKATAAQAKAAAIABIgEAyQgGgBgKAAQgaAAgOANQgNANAAAXIAABkg");
	this.shape.setTransform(77.8,30.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AhIBRQgSgSAAgjIAAh/IA2AAIAAByQAAAVAHALQAIALARAAQAVAAAKgQQAKgPAAgiIAAhcIA2AAIAADDIgpAAIgIgZIgCAAQgJAOgQAIQgPAHgUAAQgjAAgRgTg");
	this.shape_1.setTransform(56.7,30.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgxBZQgWgMgLgXQgMgXAAgfQAAgwAZgaQAZgbAtAAQAcAAAWAMQAVANAMAXQALAXAAAeQAAAwgZAbQgZAbgtAAQgcAAgVgNgAgegrQgJAOAAAdQAAAdAJAPQAKAPAUAAQAWAAAJgPQAKgPAAgdQAAgdgKgOQgJgPgWAAQgUAAgKAPg");
	this.shape_2.setTransform(33.8,30.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaCBIAAhjIhVieIA7AAIA0BqIA2hqIA6AAIhVCdIAABkg");
	this.shape_3.setTransform(13.2,27.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Your, new cjs.Rectangle(0,0,87.2,53.1), null);


(lib.Training = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgmBGQgMgHAAgPQABgJAFgHQAHgIALgCQgEgCgDgEQgDgDAAgGQAAgGADgDQAEgFAGgEQgIgDgFgIQgGgJAAgKQAAgSAMgJQAKgJATAAQAHAAAIABIAlAAIAAAKIgVADIAGAJQABAFAAAIQAAAPgKAKQgLAJgRAAIgJgBQgLAGAAAHQAAAFAEACQADABAJAAIASAAQASABAIAHQAKAHAAAOQAAASgOAJQgOAJgbAAQgVAAgLgIgAgcAhQgGAGAAAJQAAAJAHAEQAIAFANAAQAUAAAKgGQAJgGABgLQgBgIgFgEQgGgDgOAAIgSAAQgMAAgGAFgAgTg7QgGAFgBANQABALAGAGQAHAFALAAQAXABgBgXQABgZgXAAQgMAAgGAHg");
	this.shape.setTransform(100.9,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_1.setTransform(89.3,18);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIBJIAAhqIAQAAIAABqgAgGg1QgCgDAAgFQAAgGACgCQADgDADAAQAEAAADADQACACAAAGQAAAFgCADQgDACgEAAQgDAAgDgCg");
	this.shape_2.setTransform(80.6,16.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_3.setTransform(71.9,18);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgIBJIAAhqIAQAAIAABqgAgGg1QgCgDgBgFQABgGACgCQADgDADAAQAEAAADADQACACAAAGQAAAFgCADQgDACgEAAQgDAAgDgCg");
	this.shape_4.setTransform(63.3,16.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AggAvQgKgIAAgPQAAgfAzgCIASAAIAAgIQAAgMgFgGQgFgGgMAAQgNAAgRAIIgFgMQAIgEAKgDQAJgDAIABQATAAAKAIQAJAIAAAUIAABHIgMAAIgDgPIgBAAQgIALgIADQgIAEgLgBQgQABgIgJgAALABQgSABgJAGQgJAFAAALQAAAJAFAEQAGAFAKAAQANAAAJgIQAJgIAAgQIAAgJg");
	this.shape_5.setTransform(54.8,18.1);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgeA2IAAhqIAOAAIABAUIABAAQAGgKAIgGQAIgFAKAAIANAAIgCAQIgMgBQgNgBgJALQgIALgBAPIAAA4g");
	this.shape_6.setTransform(46.7,18);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgHBHIAAh+IgtAAIAAgQIBpAAIAAAQIgtAAIAAB+g");
	this.shape_7.setTransform(37.4,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Training, new cjs.Rectangle(29.8,0,78.7,31.3), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo_big();
	this.instance.parent = this;
	this.instance.setTransform(20,8,0.776,0.776);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(20,8,135.8,52), null);


(lib.Software = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgeApQgOgOgBgaQAAgZANgPQANgPAUAAQAVAAAMANQAMANgBAVIAAAKIhIAAQAAATAJAKQAJAJAQABQARAAARgIIAAAOIgQAGQgJACgKgBQgXAAgNgOgAgSghQgHAJgCAPIA3AAQAAgPgHgJQgHgIgNAAQgMAAgHAIg");
	this.shape.setTransform(112.1,18.1);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgeA2IAAhqIAOAAIACAUIAAAAQAGgKAIgGQAIgFAKAAIANAAIgCAQIgMgBQgNgBgIALQgKALABAPIAAA4g");
	this.shape_1.setTransform(103.5,18);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AggAvQgKgIAAgPQAAgfAzgCIASAAIAAgIQAAgMgFgGQgGgGgMAAQgMAAgRAIIgFgMQAIgEAJgDQAKgDAIABQAUAAAJAIQAJAIAAAUIAABHIgMAAIgDgPIgBAAQgIALgIADQgIAEgLgBQgQABgIgJgAALABQgTABgIAGQgJAFAAALQAAAJAFAEQAGAFAKAAQANAAAJgIQAJgIAAgQIAAgJg");
	this.shape_2.setTransform(92.8,18.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAbA1IgUg9IgHgbIAAAAIgGAbIgVA9IgSAAIgehqIARAAIAQA9QAGAVAAAHIABAAIAEgOIAEgOIATg9IARAAIATA9QAGARACALIAAAAIADgKIAUhPIARAAIgeBqg");
	this.shape_3.setTransform(79.8,18.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgPAiIAAg/IgQAAIAAgHIAQgHIAGgXIAJAAIAAAZIAfAAIAAAMIgfAAIAAA/QAAAJAEAGQAFAEAIAAIAIAAIAHgCIAAANQgDABgFABIgJABQgeAAAAghg");
	this.shape_4.setTransform(68.5,16.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgRBMIAAhdIgTAAIAAgHIATgGIAAgGQAAgoAiAAQAIABAMADIgFANQgJgDgHAAQgJAAgEAGQgEAGAAANIAAAHIAbAAIAAANIgbAAIAABdg");
	this.shape_5.setTransform(62.2,15.8);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgZAwQgLgGgGgNQgGgMAAgRQAAgZANgPQANgOAWAAQAXgBANAQQANAPAAAYQAAAagNAPQgNAOgXAAQgOABgLgIgAgXgeQgIAKAAAUQAAAUAIALQAIALAPAAQAQAAAIgLQAJgLAAgUQAAgUgJgKQgIgLgQAAQgPAAgIALg");
	this.shape_6.setTransform(52.1,18.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgsBDIAAgQQAJADAKADQALACAKAAQAPAAAJgGQAIgHAAgLQAAgIgDgEQgDgFgHgEQgHgEgNgFQgUgGgIgKQgJgJAAgQQAAgRAMgKQANgKATABQAVgBASAJIgFAOQgSgIgQABQgMgBgIAGQgHAGAAAKQAAAHACAFQADAFAHAEIATAIQAWAHAJAJQAIAKAAAPQAAASgOALQgNAKgXAAQgZAAgOgGg");
	this.shape_7.setTransform(40.6,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Software, new cjs.Rectangle(33.1,0,86.7,31.3), null);


(lib.Engineering = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AhMCBQgWgOAAgaQAAgSALgMQALgMAWgFQgJgEgGgHQgGgJAAgIQAAgMAGgGQAHgIAMgHQgPgHgJgOQgJgPAAgUQAAggAVgSQAUgRAoAAIASABIAOACIBFAAIAAAbIgeAIQAIAOAAAQQAAAggWARQgXASgmAAIgJgBIgIgBQgJAHAAAHQAAALAeAAIAgAAQAhAAARAOQARAOAAAbQAAAigdATQgdATg1AAQgpAAgVgOgAgqBCQgKAHAAAMQAAALALAGQALAGATAAQAbAAARgIQAQgIAAgNQAAgLgKgFQgJgEgUAAIgbAAQgPAAgKAHgAgghKQAAAQAHAJQAHAJAQAAQANAAAHgJQAIgJAAgQQAAgkgcAAQgeAAAAAkg");
	this.shape.setTransform(204.3,34.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAlBkIAAhyQAAgVgHgLQgIgLgQAAQgWAAgKAQQgKAPAAAhIAABdIg2AAIAAjDIApAAIAIAYIADAAQAIgOAQgHQAPgHAUAAQAiAAASASQASATAAAjIAAB/g");
	this.shape_1.setTransform(182.5,30.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgaCJIAAjEIA1AAIAADEgAgchuQAAgaAcAAQAdAAAAAaQAAANgHAHQgHAHgPAAQgcAAAAgbg");
	this.shape_2.setTransform(165.1,26.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ag/BkIAAjDIApAAIAIAgIACAAQAKgQAOgKQAQgKASAAQAKAAAIABIgEAyQgHgBgJAAQgaAAgOANQgOANAAAXIAABkg");
	this.shape_3.setTransform(152.5,30.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("Ag/BMQgbgbAAgwQAAgwAZgbQAZgbArAAQApAAAYAYQAXAXgBAqIAAAZIh9AAQABAXANANQAMANAWAAQASAAAQgEQAQgEAQgIIAAAqQgOAHgPADQgRAEgWAAQgvAAgbgagAgWg0QgLALgBAUIBLAAQgBgUgKgLQgKgLgRAAQgQAAgJALg");
	this.shape_4.setTransform(132.7,30.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("Ag/BMQgbgbAAgwQAAgwAZgbQAZgbArAAQAqAAAXAYQAWAXAAAqIAAAZIh9AAQABAXANANQAMANAWAAQASAAAPgEQARgEAQgIIAAAqQgOAHgPADQgQAEgYAAQguAAgbgagAgWg0QgLALgBAUIBLAAQgBgUgKgLQgKgLgRAAQgQAAgJALg");
	this.shape_5.setTransform(111.4,30.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AAlBkIAAhyQAAgVgHgLQgIgLgQAAQgWAAgKAQQgKAPAAAhIAABdIg2AAIAAjDIApAAIAIAYIADAAQAIgOAQgHQAPgHAUAAQAiAAASASQASATAAAjIAAB/g");
	this.shape_6.setTransform(89,30.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaCJIAAjEIA1AAIAADEgAgchuQAAgaAcAAQAdAAAAAaQAAANgHAHQgIAHgOAAQgcAAAAgbg");
	this.shape_7.setTransform(71.6,26.8);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AhMCBQgWgOAAgaQAAgSALgMQALgMAVgFQgHgEgHgHQgGgJAAgIQAAgMAHgGQAGgIANgHQgQgHgJgOQgJgPAAgUQAAggAUgSQAVgRAnAAIATABIAPACIBEAAIAAAbIgfAIQAJAOAAAQQAAAggWARQgXASglAAIgKgBIgIgBQgIAHAAAHQgBALAeAAIAhAAQAgAAARAOQARAOAAAbQAAAigdATQgdATg0AAQgpAAgWgOgAgpBCQgKAHAAAMQAAALAKAGQAKAGAUAAQAbAAARgIQARgIAAgNQAAgLgKgFQgKgEgUAAIgbAAQgOAAgKAHgAgghKQAAAQAIAJQAGAJAPAAQAOAAAIgJQAGgJAAgQQAAgkgcAAQgdAAAAAkg");
	this.shape_8.setTransform(55.8,34.8);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AAlBkIAAhyQAAgVgHgLQgIgLgQAAQgWAAgKAQQgKAPAAAhIAABdIg2AAIAAjDIApAAIAIAYIADAAQAIgOAQgHQAPgHAUAAQAiAAASASQASATAAAjIAAB/g");
	this.shape_9.setTransform(34,30.5);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AhJCBIAAkBICTAAIAAAtIhcAAIAAA5IBWAAIAAArIhWAAIAABDIBcAAIAAAtg");
	this.shape_10.setTransform(12.7,27.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Engineering, new cjs.Rectangle(0,0,216.8,53.1), null);


(lib.Consulting = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgmBGQgMgHAAgPQABgJAFgHQAHgIALgCQgEgCgDgEQgCgDAAgGQAAgGACgDQADgFAHgEQgIgDgFgIQgFgJgBgKQABgSAKgJQALgJATAAQAIAAAGABIAmAAIAAAKIgVADIAGAJQABAFAAAIQAAAPgKAKQgLAJgRAAIgJgBQgLAGAAAHQAAAFAEACQADABAJAAIASAAQASABAIAHQAKAHAAAOQAAASgOAJQgOAJgbAAQgVAAgLgIgAgcAhQgGAGAAAJQAAAJAIAEQAGAFAOAAQAUAAAKgGQAJgGABgLQAAgIgGgEQgGgDgOAAIgSAAQgMAAgGAFgAgTg7QgGAFAAANQAAALAGAGQAHAFALAAQAWABAAgXQAAgZgWAAQgMAAgGAHg");
	this.shape.setTransform(95.8,20.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_1.setTransform(84.2,18);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgIBJIAAhqIAQAAIAABqgAgGg1QgDgDABgFQgBgGADgCQADgDADAAQAEAAADADQADACAAAGQAAAFgDADQgDACgEAAQgDAAgDgCg");
	this.shape_2.setTransform(75.6,16.1);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgPAiIAAg/IgQAAIAAgHIAQgHIAGgXIAJAAIAAAZIAfAAIAAAMIgfAAIAAA/QAAAJAEAGQAFAEAIAAIAIAAIAHgCIAAANQgDABgFABIgJABQgeAAAAghg");
	this.shape_3.setTransform(69.4,16.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHBMIAAiXIAPAAIAACXg");
	this.shape_4.setTransform(63.4,15.8);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgiAtQgKgJAAgUIAAhGIARAAIAABFQAAANAGAHQAGAGAMAAQAQAAAIgJQAIgJAAgWIAAg3IAQAAIAABqIgOAAIgCgOIgBAAQgFAIgJAFQgIADgLAAQgTAAgKgJg");
	this.shape_5.setTransform(54.7,18.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AglAwIAAgOQAIAEAJACQAJACAIAAQAMAAAHgEQAGgEAAgIQAAgGgFgFQgFgEgPgGQgPgFgGgEQgHgEgDgGQgDgFAAgHQAAgNALgIQAKgHASAAQARgBARAIIgGAMQgQgGgNAAQgLAAgGAEQgFADAAAHQAAAEACADQACADAFADIASAHQATAHAGAHQAHAGAAAMQAAAOgLAIQgLAJgTgBQgWABgMgIg");
	this.shape_6.setTransform(43.9,18.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAdA2IAAhEQAAgNgGgHQgGgFgNgBQgQABgIAIQgHAJAAAWIAAA2IgRAAIAAhqIAOAAIACAPIABAAQAFgIAJgEQAJgEAKAAQATAAAKAJQAKAJAAAUIAABFg");
	this.shape_7.setTransform(32.9,18);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgZAwQgLgGgGgNQgGgMAAgRQAAgZANgPQANgOAWAAQAXgBANAQQANAPAAAYQAAAagNAPQgNAOgXAAQgOABgLgIgAgXgeQgIAKAAAUQAAAUAIALQAIALAPAAQAQAAAIgLQAJgLAAgUQAAgUgJgKQgIgLgQAAQgPAAgIALg");
	this.shape_8.setTransform(20.7,18.1);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgjA2QgSgTAAgjQAAgVAJgRQAHgQAQgJQAQgKAUABQAWAAARAIIgHAOQgQgIgQABQgXAAgOAPQgNAQAAAaQAAAcANAQQANAPAXAAQAQgBATgEIAAAOQgPAGgWgBQgeABgRgUg");
	this.shape_9.setTransform(8.7,16.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Consulting, new cjs.Rectangle(0,0,103.4,31.3), null);


(lib.CoPilot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgbBpQgPgRAAggIAAheIgZAAIAAgXIAegSIAPgpIAhAAIAAAqIA3AAIAAAoIg3AAIAABeQAAAMAGAFQAHAGALAAQAOAAATgHIAAAoQgUAJgdAAQgfAAgPgQg");
	this.shape.setTransform(133.4,28.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgxBZQgWgMgLgXQgMgXAAgfQAAgwAZgaQAZgbAtAAQAcAAAWAMQAVANAMAXQALAXAAAeQAAAwgZAbQgZAbgtAAQgcAAgVgNgAgegrQgJAOAAAdQAAAdAJAPQAKAPAUAAQAWAAAJgPQAKgPAAgdQAAgdgKgOQgJgPgWAAQgUAAgKAPg");
	this.shape_1.setTransform(114.5,30.7);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgaCJIAAkRIA1AAIAAERg");
	this.shape_2.setTransform(97.9,26.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgaCJIAAjEIA0AAIAADEgAgchuQAAgaAcAAQAdAAAAAaQAAANgHAHQgIAHgOAAQgcAAAAgbg");
	this.shape_3.setTransform(86.9,26.8);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AhYCBIAAkBIBSAAQAuAAAYAVQAZAUAAAnQAAAqgaAVQgaAWgvAAIgXAAIAABcgAghgGIASAAQAYAAAMgKQANgLAAgSQAAgUgLgJQgKgJgWAAIgYAAg");
	this.shape_4.setTransform(71,27.7);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AguAWIAAgrIBdAAIAAArg");
	this.shape_5.setTransform(53,30.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgxBZQgWgMgLgXQgMgXAAgfQAAgwAZgaQAZgbAtAAQAcAAAWAMQAVANAMAXQALAXAAAeQAAAwgZAbQgZAbgtAAQgcAAgVgNgAgegrQgJAOAAAdQAAAdAJAPQAKAPAUAAQAWAAAJgPQAKgPAAgdQAAgdgKgOQgJgPgWAAQgUAAgKAPg");
	this.shape_6.setTransform(36.1,30.7);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AhDBiQgegiAAg/QAAgnAPgfQAOgeAcgQQAbgQAkAAQAlAAAmASIgSAsIgdgLQgOgGgOAAQgeAAgRAYQgRAWAAApQAABVBAAAQAbAAAmgNIAAAuQgfANgnAAQg3AAgegig");
	this.shape_7.setTransform(13.9,27.7);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.CoPilot, new cjs.Rectangle(0,0,143.6,53.1), null);


(lib.arrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#53BCEB").s().p("AgJBEIAohDIgohEIArAAIAmBEIgkBDgAhHBEIAphDIgphEIAsAAIAlBEIgjBDg");
	this.shape.setTransform(7.2,6.8);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.arrow, new cjs.Rectangle(0,0,14.5,13.5), null);


// stage content:
(lib.GSC1879_ABMBannerAd_728x90 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Training
	this.instance = new lib.Training();
	this.instance.parent = this;
	this.instance.setTransform(344.1,76,1,1,0,0,0,69.1,26.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(37).to({_off:false},0).wait(1).to({regX:69,regY:18.4,x:340.4,y:67.9,alpha:0.25},0).wait(1).to({x:336.8,alpha:0.5},0).wait(1).to({x:333.1,alpha:0.75},0).wait(1).to({x:329.5,alpha:1},0).wait(25).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(37).to({_off:false,regX:69.1,regY:26.5,x:344.1,y:76},0).wait(1).to({regX:69,regY:18.4,x:340.4,y:67.9,alpha:0.25},0).wait(1).to({x:336.8,alpha:0.5},0).wait(1).to({x:333.1,alpha:0.75},0).wait(1).to({x:329.5,alpha:1},0).wait(21));

	// Arrow 3
	this.instance_1 = new lib.arrow();
	this.instance_1.parent = this;
	this.instance_1.setTransform(264.9,71.9,1,1,0,0,0,13.2,12.6);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(37).to({_off:false},0).wait(1).to({regX:7.2,regY:6.8,x:265.2,y:66.1,alpha:0.25},0).wait(1).to({x:271.4,alpha:0.5},0).wait(1).to({x:277.7,alpha:0.75},0).wait(1).to({x:283.9,alpha:1},0).wait(25).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(37).to({_off:false,regX:13.2,regY:12.6,x:264.9,y:71.9},0).wait(1).to({regX:7.2,regY:6.8,x:265.2,y:66.1,alpha:0.25},0).wait(1).to({x:271.4,alpha:0.5},0).wait(1).to({x:277.7,alpha:0.75},0).wait(1).to({x:283.9,alpha:1},0).wait(21));

	// Software
	this.instance_2 = new lib.Software();
	this.instance_2.parent = this;
	this.instance_2.setTransform(235.4,76,1,1,0,0,0,76.4,26.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(33).to({_off:false},0).wait(1).to({regY:15.9,x:231.9,y:65.4,alpha:0.25},0).wait(1).to({x:228.4,alpha:0.5},0).wait(1).to({x:224.9,alpha:0.75},0).wait(1).to({x:221.4,alpha:1},0).wait(29).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(33).to({_off:false,regY:26.5,x:235.4,y:76},0).wait(1).to({regY:15.9,x:231.9,y:65.4,alpha:0.25},0).wait(1).to({x:228.4,alpha:0.5},0).wait(1).to({x:224.9,alpha:0.75},0).wait(1).to({x:221.4,alpha:1},0).wait(25));

	// Arrow 2
	this.instance_3 = new lib.arrow();
	this.instance_3.parent = this;
	this.instance_3.setTransform(149.6,71.8,1,1,0,0,0,13.2,12.6);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(33).to({_off:false},0).wait(1).to({regX:7.2,regY:6.8,x:150.1,y:66,alpha:0.25},0).wait(1).to({x:156.6,alpha:0.5},0).wait(1).to({x:163.1,alpha:0.75},0).wait(1).to({x:169.6,alpha:1},0).wait(29).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(33).to({_off:false,regX:13.2,regY:12.6,x:149.6,y:71.8},0).wait(1).to({regX:7.2,regY:6.8,x:150.1,y:66,alpha:0.25},0).wait(1).to({x:156.6,alpha:0.5},0).wait(1).to({x:163.1,alpha:0.75},0).wait(1).to({x:169.6,alpha:1},0).wait(25));

	// Consulting
	this.instance_4 = new lib.Consulting();
	this.instance_4.parent = this;
	this.instance_4.setTransform(153.3,76,1,1,0,0,0,91.3,26.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(29).to({_off:false},0).wait(1).to({regX:52,regY:18.2,x:110.7,y:67.7,alpha:0.25},0).wait(1).to({x:107.5,alpha:0.5},0).wait(1).to({x:104.2,alpha:0.75},0).wait(1).to({x:101,alpha:1},0).wait(33).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(29).to({_off:false,regX:91.3,regY:26.5,x:153.3,y:76},0).wait(1).to({regX:52,regY:18.2,x:110.7,y:67.7,alpha:0.25},0).wait(1).to({x:107.5,alpha:0.5},0).wait(1).to({x:104.2,alpha:0.75},0).wait(1).to({x:101,alpha:1},0).wait(29));

	// Arrow 1
	this.instance_5 = new lib.arrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(20.7,71.8,1,1,0,0,0,13.2,12.6);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(29).to({_off:false},0).wait(1).to({regX:7.2,regY:6.8,x:21,y:66,alpha:0.25},0).wait(1).to({x:27.2,alpha:0.5},0).wait(1).to({x:33.5,alpha:0.75},0).wait(1).to({x:39.7,alpha:1},0).wait(33).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(29).to({_off:false,regX:13.2,regY:12.6,x:20.7,y:71.8},0).wait(1).to({regX:7.2,regY:6.8,x:21,y:66,alpha:0.25},0).wait(1).to({x:27.2,alpha:0.5},0).wait(1).to({x:33.5,alpha:0.75},0).wait(1).to({x:39.7,alpha:1},0).wait(29));

	// Co-Pilot
	this.instance_6 = new lib.CoPilot();
	this.instance_6.parent = this;
	this.instance_6.setTransform(478.3,34.9,1,1,0,0,0,87.3,31.9);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(17).to({_off:false},0).wait(1).to({regX:72.1,regY:27,x:451.8,y:30,alpha:0.25},0).wait(1).to({x:440.6,alpha:0.5},0).wait(1).to({x:429.3,alpha:0.75},0).wait(1).to({x:418.1,alpha:1},0).wait(45).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(17).to({_off:false,regX:87.3,regY:31.9,x:478.3,y:34.9},0).wait(1).to({regX:72.1,regY:27,x:451.8,y:30,alpha:0.25},0).wait(1).to({x:440.6,alpha:0.5},0).wait(1).to({x:429.3,alpha:0.75},0).wait(1).to({x:418.1,alpha:1},0).wait(41));

	// Engineering
	this.instance_7 = new lib.Engineering();
	this.instance_7.parent = this;
	this.instance_7.setTransform(285.1,35.4,1,1,0,0,0,132.1,31.9);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(13).to({_off:false},0).wait(1).to({regX:109.7,regY:31.1,x:255,y:34.6,alpha:0.25},0).wait(1).to({x:247.4,alpha:0.5},0).wait(1).to({x:239.8,alpha:0.75},0).wait(1).to({x:232.2,alpha:1},0).wait(49).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(13).to({_off:false,regX:132.1,regY:31.9,x:285.1,y:35.4},0).wait(1).to({regX:109.7,regY:31.1,x:255,y:34.6,alpha:0.25},0).wait(1).to({x:247.4,alpha:0.5},0).wait(1).to({x:239.8,alpha:0.75},0).wait(1).to({x:232.2,alpha:1},0).wait(45));

	// Your
	this.instance_8 = new lib.Your();
	this.instance_8.parent = this;
	this.instance_8.setTransform(111.3,36.3,1,1,0,0,0,52.8,31.9);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(9).to({_off:false},0).wait(1).to({regX:43.1,regY:27.8,x:93.7,y:32.2,alpha:0.25},0).wait(1).to({x:85.9,alpha:0.5},0).wait(1).to({x:78,alpha:0.75},0).wait(1).to({x:70.1,alpha:1},0).wait(53).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(9).to({_off:false,regX:52.8,regY:31.9,x:111.3,y:36.3},0).wait(1).to({regX:43.1,regY:27.8,x:93.7,y:32.2,alpha:0.25},0).wait(1).to({x:85.9,alpha:0.5},0).wait(1).to({x:78,alpha:0.75},0).wait(1).to({x:70.1,alpha:1},0).wait(49));

	// logo
	this.instance_9 = new lib.Symbol1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(631.5,45.5,1,1,0,0,0,87.5,33.5);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({regX:87.9,regY:34,x:631.9,y:46,alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(62).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).wait(1).to({regX:87.5,regY:33.5,x:631.5,y:45.5},0).wait(1).to({regX:87.9,regY:34,x:631.9,y:46,alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(58));

	// Backgound
	this.instance_10 = new lib.GSC1879_ABMBannerAd_728x90_1();
	this.instance_10.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(133));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(364,45,728,90);
// library properties:
lib.properties = {
	id: 'D4C83844DFB7CD4CA2819D590FAC090D',
	width: 728,
	height: 90,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/GSC1879_ABM_Banner_Ad_728x90_atlas_.png", id:"GSC1879_ABM_Banner_Ad_728x90_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D4C83844DFB7CD4CA2819D590FAC090D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;