(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"GSC1879_ABM_Banner_Ad_300x50_atlas_", frames: [[98,252,15,15],[0,0,300,250],[0,252,96,37]]}
];


// symbols:



(lib.arow = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x50_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.bg = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x50_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.logo = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_300x50_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Symbol10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgcAsIAAhWIASAAIAEAPIABAAQAFgIAFgEQAHgEAJgBIAIABIgDAXIgGgBQgMAAgGAFQgGAHABAJIAAAsg");
	this.shape.setTransform(20.2,8.2);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgfAlQgIgJAAgQIAAg3IAYAAIAAAxQAAAKADAFQADAFAIAAQAJAAAEgHQAFgHAAgOIAAgpIAXAAIAABWIgSAAIgDgLIgBAAQgEAGgHADQgHAEgIAAQgQgBgHgHg");
	this.shape_1.setTransform(10.8,8.3);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgVAoQgKgGgFgKQgGgKAAgOQAAgUAMgMQALgMATAAQANAAAKAFQAJAGAFAKQAFAKABANQAAAVgMAMQgLAMgUAAQgMAAgJgFgAgNgTQgEAHAAAMQAAANAFAHQADAGAJAAQAJAAAFgGQAEgHAAgNQAAgMgEgHQgFgGgJAAQgJAAgEAGg");
	this.shape_2.setTransform(0.6,8.2);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgLA5IAAgrIgmhGIAbAAIAWAvIAYgvIAaAAIgmBFIAAAsg");
	this.shape_3.setTransform(-8.5,6.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol10, new cjs.Rectangle(-15.5,-6.5,41.1,25.8), null);


(lib.Symbol9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgiA5QgJgGAAgMQAAgHAFgGQAFgFAJgCQgEgCgCgDQgDgEAAgEQAAgFADgCQADgEAFgDQgGgDgFgGQgDgHAAgJQAAgOAJgIQAJgIASAAIAHABIAGABIAfAAIAAAMIgOAEQAEAFAAAIQAAAOgJAIQgLAHgQAAIgEAAIgDAAQgFADAAACQAAAFAOAAIANAAQAPAAAIAGQAHAHAAAMQAAAPgNAIQgNAJgXAAQgRAAgLgHgAgSAdQgFAEAAAFQABAFAFACQAEADAIAAQAMAAAIgDQAHgEAAgGQAAgFgEgCQgEgCgKAAIgLAAQgGAAgFADgAgOggQAAAHAEAEQADAEAHAAQAFAAADgEQAEgEgBgHQABgQgMAAQgOAAAAAQg");
	this.shape.setTransform(117.4,-25.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AARAsIAAgyQAAgJgEgEQgDgGgIAAQgJAAgEAHQgEAHAAAOIAAApIgYAAIAAhWIASAAIADAMIABAAQAEgHAIgDQAFgDAJgBQAQAAAHAJQAIAIAAAQIAAA3g");
	this.shape_1.setTransform(107.7,-27.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA9IAAhXIAXAAIAABXgAgMgwQAAgMAMAAQANAAAAAMQAAAFgDAEQgEADgGAAQgMAAAAgMg");
	this.shape_2.setTransform(100,-29);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgcAsIAAhWIASAAIAEAPIABAAQAFgIAFgEQAHgEAJgBIAIABIgDAXIgGgBQgMAAgGAFQgGAHABAJIAAAsg");
	this.shape_3.setTransform(94.4,-27.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgbAiQgMgMAAgVQAAgVALgMQALgMASAAQATAAAKAKQAKALAAASIAAALIg3AAQAAAKAGAGQAGAGAJAAQAIAAAHgCQAHgCAHgDIAAASQgGADgHACQgHABgKAAQgUAAgMgLgAgJgWQgFAEAAAJIAgAAQAAgJgFgEQgEgFgIAAQgGAAgEAFg");
	this.shape_4.setTransform(85.6,-27.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgbAiQgMgMAAgVQAAgVALgMQALgMASAAQATAAAKAKQAKALAAASIAAALIg3AAQAAAKAGAGQAGAGAJAAQAIAAAHgCQAHgCAHgDIAAASQgGADgHACQgHABgKAAQgUAAgMgLgAgJgWQgFAEAAAJIAgAAQAAgJgFgEQgEgFgIAAQgGAAgEAFg");
	this.shape_5.setTransform(76.1,-27.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AARAsIAAgyQAAgJgEgEQgDgGgIAAQgJAAgEAHQgEAHAAAOIAAApIgYAAIAAhWIASAAIADAMIABAAQAEgHAIgDQAFgDAJgBQAQAAAHAJQAIAIAAAQIAAA3g");
	this.shape_6.setTransform(66.2,-27.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgLA9IAAhXIAXAAIAABXgAgMgwQAAgMAMAAQANAAAAAMQAAAFgEAEQgCADgHAAQgMAAAAgMg");
	this.shape_7.setTransform(58.5,-29);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AghA5QgKgGAAgMQAAgHAFgGQAFgFAKgCQgEgCgDgDQgDgEAAgEQAAgFADgCQADgEAFgDQgGgDgEgGQgEgHgBgJQAAgOAKgIQAJgIARAAIAIABIAGABIAfAAIAAAMIgNAEQADAFAAAIQAAAOgKAIQgJAHgRAAIgEAAIgEAAQgEADABACQgBAFANAAIAOAAQAPAAAIAGQAHAHAAAMQAAAPgMAIQgNAJgXAAQgSAAgKgHgAgSAdQgEAEgBAFQAAAFAGACQAEADAIAAQANAAAGgDQAIgEAAgGQAAgFgEgCQgFgCgJAAIgLAAQgHAAgEADgAgNggQAAAHACAEQAEAEAGAAQAGAAADgEQADgEABgHQgBgQgMAAQgNAAABAQg");
	this.shape_8.setTransform(51.4,-25.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AARAsIAAgyQAAgJgEgEQgDgGgIAAQgJAAgEAHQgEAHAAAOIAAApIgYAAIAAhWIASAAIADAMIABAAQAEgHAIgDQAFgDAJgBQAQAAAHAJQAIAIAAAQIAAA3g");
	this.shape_9.setTransform(41.7,-27.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AggA5IAAhxIBBAAIAAATIgpAAIAAAaIAmAAIAAASIgmAAIAAAeIApAAIAAAUg");
	this.shape_10.setTransform(32.2,-28.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol9, new cjs.Rectangle(25.5,-64.5,108.3,54.4), null);


(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMAvQgGgIAAgOIAAgpIgMAAIAAgKIAOgIIAHgTIAOAAIAAATIAYAAIAAASIgYAAIAAApQAAAFADACQACADAGAAQAGAAAIgDIAAASQgIAEgNAAQgOAAgHgHg");
	this.shape.setTransform(327.5,-67.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgVAoQgKgGgFgKQgGgKAAgOQAAgUAMgMQALgMATAAQANAAAKAFQAJAGAFAKQAFAKABANQAAAVgMAMQgLAMgUAAQgMAAgJgFgAgNgTQgEAHAAAMQAAANAFAHQADAGAJAAQAJAAAFgGQAEgHAAgNQAAgMgEgHQgFgGgJAAQgJAAgEAGg");
	this.shape_1.setTransform(319.1,-66.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLA9IAAh5IAXAAIAAB5g");
	this.shape_2.setTransform(311.7,-68.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgLA9IAAhXIAXAAIAABXgAgMgwQAAgMAMAAQANAAAAAMQAAAFgEAEQgDADgGAAQgMAAAAgMg");
	this.shape_3.setTransform(306.8,-68.5);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgnA5IAAhxIAlAAQATAAAMAJQAKAJABARQAAATgMAJQgMAJgUABIgKAAIAAAogAgOgDIAIAAQAJABAHgFQAFgFAAgHQAAgJgFgFQgEgDgJAAIgLAAg");
	this.shape_4.setTransform(299.7,-68.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgUAKIAAgTIApAAIAAATg");
	this.shape_5.setTransform(291.7,-66.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgVAoQgKgGgFgKQgGgKABgOQgBgUAMgMQALgMATAAQANAAAKAFQAJAGAFAKQAFAKAAANQAAAVgLAMQgLAMgUAAQgMAAgJgFgAgNgTQgEAHAAAMQAAANAFAHQADAGAJAAQAJAAAFgGQAEgHAAgNQAAgMgEgHQgFgGgJAAQgIAAgFAGg");
	this.shape_6.setTransform(284.2,-66.8);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgeAsQgNgPAAgdQABgRAGgNQAHgNAMgHQAMgIAPAAQAQAAASAIIgIAUIgOgFQgGgCgGAAQgNAAgHAKQgIAKAAARQAAAnAcAAQAMAAARgGIAAAUQgOAGgRAAQgZAAgNgPg");
	this.shape_7.setTransform(274.4,-68.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol8, new cjs.Rectangle(267.1,-81.5,66.1,25.8), null);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTAjQgFgEAAgHQAAgFADgDQADgEAGgBIgEgDQAAAAAAgBQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgDABgBQACgDADgCQgEgBgDgFQgCgEAAgFQAAgJAFgEQAFgFAKAAIAHABIASAAIAAAFIgKABIADAFIABAGQAAAIgGAEQgFAFgIAAIgFAAQgFACAAADQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAIAGABIAIAAQAJAAAFADQAEAEAAAHQAAAJgHAFQgHAEgMAAQgLAAgGgEgAgNARQgEACAAAFQAAAFAEACQAEACAHAAQAJAAAFgDQAFgDAAgFQAAgFgDgBQgDgCgHAAIgJAAQgFAAgDADgAgJgdQgDADAAAGQAAAFADADQADADAGAAQALAAAAgLQAAgMgLAAQgGAAgDADg");
	this.shape.setTransform(147.6,-163.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAOAbIAAghQAAgHgDgDQgCgDgHAAQgHAAgFAEQgDAFgBAKIAAAbIgIAAIAAg0IAHAAIABAHIABAAQADgEAEgCQAFgCAEAAQAJAAAGAEQAEAFAAAKIAAAig");
	this.shape_1.setTransform(141.9,-165.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgDAlIAAg1IAHAAIAAA1gAgCgaQgBgBAAAAQAAAAAAgBQgBAAAAgBQAAgBAAAAQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAQAAgBAAAAQABAAAAAAQAAgBABAAQAAAAAAAAQAAAAABAAQAAAAABABQAAAAAAAAQABAAAAABQABAAAAAAQAAABAAAAQABABAAAAQAAABAAABQAAAAAAABQAAABgBAAQAAABAAAAQAAAAgBABQAAAAgBAAQAAABAAAAQgBAAAAAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAg");
	this.shape_2.setTransform(137.5,-166);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAOAbIAAghQAAgHgDgDQgDgDgGAAQgIAAgDAEQgFAFAAAKIAAAbIgIAAIAAg0IAHAAIABAHIABAAQACgEAFgCQAEgCAFAAQAJAAAGAEQAEAFAAAKIAAAig");
	this.shape_3.setTransform(133.2,-165.1);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgDAlIAAg1IAHAAIAAA1gAgCgaQgBgBAAAAQAAAAAAgBQgBAAAAgBQAAgBAAAAQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAQAAgBAAAAQABAAAAAAQAAgBABAAQAAAAAAAAQAAAAABAAQAAAAABABQAAAAAAAAQABAAAAABQABAAAAAAQAAABAAAAQABABAAAAQAAABAAABQAAAAAAABQAAABgBAAQAAABAAAAQAAAAgBABQAAAAgBAAQAAABAAAAQgBAAAAAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAg");
	this.shape_4.setTransform(128.8,-166);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgQAYQgEgEAAgIQAAgPAZgBIAJAAIAAgEQgBgGgCgDQgDgDgGAAQgFAAgJAEIgCgGIAJgDIAIgCQAJAAAFAFQAEAEABAJIAAAkIgHAAIgBgIIAAAAQgEAFgFACQgEACgEAAQgJAAgEgEgAAFABQgIAAgFADQgEACAAAGQAAAEADADQACACAFAAQAHAAAEgEQAEgEABgIIAAgFg");
	this.shape_5.setTransform(124.6,-165);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPAbIAAg0IAIAAIAAAKIAAAAQADgGAEgCQAEgDAFAAIAHAAIgCAIIgFgBQgHAAgDAFQgGAGAAAIIAAAbg");
	this.shape_6.setTransform(120.5,-165.1);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgDAkIAAg/IgXAAIAAgIIA1AAIAAAIIgXAAIAAA/g");
	this.shape_7.setTransform(115.8,-165.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol7, new cjs.Rectangle(111,-175,41.6,17.7), null);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgPAVQgHgIAAgNQAAgMAHgHQAGgIAKAAQAKAAAGAHQAGAGAAALIAAAEIgkAAQAAAKAFAFQAEAFAHAAQAJAAAIgEIAAAHIgIADIgJABQgLAAgHgHgAgIgQQgEAEgBAIIAbAAQAAgIgDgEQgEgEgGAAQgGAAgDAEg");
	this.shape.setTransform(87.6,-136);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgOAbIAAg0IAHAAIAAAKIABAAQADgGADgCQAEgDAFAAIAGAAIgBAIIgGgBQgGAAgEAFQgEAGAAAIIAAAbg");
	this.shape_1.setTransform(83.3,-136.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgQAYQgFgEABgIQAAgPAZgBIAJAAIAAgEQAAgGgDgDQgDgDgGAAQgGAAgIAEIgDgGIAJgDIAIgCQALAAAEAFQAFAEgBAJIAAAkIgFAAIgCgIIAAAAQgFAFgDACQgFACgEAAQgJAAgEgEgAAGABQgKAAgEADQgEACAAAGQAAAEADADQADACAEAAQAHAAAEgEQAFgEAAgIIAAgFg");
	this.shape_2.setTransform(77.9,-136);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAOAbIgKgfIgEgNIAAAAIgDANIgKAfIgJAAIgPg1IAJAAIAIAeIADAOIAAAAIACgHIACgHIAKgeIAIAAIAJAeIAEAOIAAAAIABgFIALgnIAIAAIgPA1g");
	this.shape_3.setTransform(71.4,-136);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgHARIAAgfIgIAAIAAgDIAIgEIADgMIAEAAIAAANIAQAAIAAAGIgQAAIAAAfQAAAEACADQADADAEAAIAEAAIADgBIAAAGIgEABIgFAAQgOAAAAgQg");
	this.shape_4.setTransform(65.7,-136.6);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgIAmIAAguIgJAAIAAgEIAJgDIAAgDQAAgTAQAAIAKABIgCAHIgIgCQgEAAgCAEQgCADAAAGIAAAEIANAAIAAAGIgNAAIAAAug");
	this.shape_5.setTransform(62.5,-137.2);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgMAYQgGgDgDgGQgDgHAAgIQAAgMAHgHQAGgIALAAQALAAAHAIQAHAHAAAMQAAANgHAHQgHAIgLAAQgGAAgGgEgAgLgPQgEAGAAAJQAAAKAEAGQAEAFAHAAQAIAAAEgFQAEgGAAgKQAAgJgEgGQgEgFgIAAQgHAAgEAFg");
	this.shape_6.setTransform(57.5,-136);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgWAhIAAgIIAKADIAKABQAIAAAEgDQAEgDAAgFQAAgEgBgDIgFgEIgKgEQgKgDgEgFQgEgFgBgIQABgIAGgFQAGgFAJAAQALAAAJAEIgDAHQgJgDgIAAQgFAAgFADQgDACAAAFQAAAEABADQABACAEACIAJAEQALADAFAFQADAEABAIQgBAJgGAFQgHAGgLAAQgMAAgIgEg");
	this.shape_7.setTransform(51.8,-136.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol6, new cjs.Rectangle(47,-146,45.6,17.7), null);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgTAjQgFgEAAgHQAAgFADgDQADgEAGgBIgEgDQAAAAAAgBQgBAAAAgBQAAAAAAgBQAAgBAAAAQAAgDABgBQACgDADgCQgEgBgDgFQgCgEAAgFQAAgJAFgEQAFgFAKAAIAHABIASAAIAAAFIgKABIADAFIABAGQAAAIgGAEQgFAFgIAAIgFAAQgFACAAADQAAABAAAAQAAABAAAAQABABAAAAQAAABABAAIAGABIAIAAQAJAAAFADQAEAEAAAHQAAAJgHAFQgHAEgMAAQgLAAgGgEgAgNARQgEACAAAFQAAAFAEACQAEACAHAAQAJAAAFgDQAFgDAAgFQAAgFgDgBQgDgCgHAAIgJAAQgFAAgDADgAgJgdQgDADAAAGQAAAFADADQADADAGAAQALAAAAgLQAAgMgLAAQgGAAgDADg");
	this.shape.setTransform(23.4,-105.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AAOAbIAAghQAAgHgDgDQgCgDgHAAQgHAAgFAEQgEAFAAAKIAAAbIgIAAIAAg0IAHAAIABAHIABAAQADgEAEgCQAFgCAEAAQAJAAAGAEQAEAFAAAKIAAAig");
	this.shape_1.setTransform(17.7,-107.1);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgDAlIAAg1IAHAAIAAA1gAgCgaQgBgBAAAAQAAgBAAAAQgBgBAAAAQAAgBAAAAQAAgBAAgBQAAAAABgBQAAAAAAgBQAAAAABAAQAAgBAAAAQABAAAAAAQAAgBABAAQAAAAAAAAQAAAAABAAQAAAAABABQAAAAAAAAQABAAAAABQABAAAAAAQAAABAAAAQABABAAAAQAAABAAABQAAAAAAABQAAAAgBABQAAAAAAABQAAAAgBABQAAAAgBAAQAAABAAAAQgBAAAAAAQgBAAAAAAQAAAAAAAAQgBAAAAAAQAAAAgBgBQAAAAAAAAg");
	this.shape_2.setTransform(13.3,-108);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgHARIAAgfIgIAAIAAgDIAIgEIADgMIAEAAIAAANIAQAAIAAAGIgQAAIAAAfQAAAFACACQADADAEAAIAEAAIADgBIAAAGIgEABIgFABQgOAAAAgRg");
	this.shape_3.setTransform(10.2,-107.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgDAmIAAhLIAHAAIAABLg");
	this.shape_4.setTransform(7.2,-108.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgQAXQgGgFAAgKIAAgiIAJAAIAAAhQAAAHACADQAEADAGAAQAHAAAFgEQADgFAAgKIAAgbIAIAAIAAA0IgHAAIgBgHIAAAAQgDAEgEACQgEACgFAAQgKAAgEgEg");
	this.shape_5.setTransform(2.8,-107);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgRAYIAAgHIAHADIAJABQAFAAAEgCQADgCABgEQAAgDgDgCQgDgDgHgDIgLgEIgEgEQgCgDAAgEQAAgGAFgEQAGgEAIAAQAIAAAJAEIgDAGQgIgDgGAAQgFAAgDACQgDACAAADIABADQAAABABAAQAAABAAAAQABAAAAABQABAAABAAIAIAEQAKADADADQADAEAAAFQAAAIgGAEQgFAEgJAAQgLAAgFgEg");
	this.shape_6.setTransform(-2.6,-107);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AAOAbIAAghQAAgHgDgDQgDgDgGAAQgIAAgDAEQgEAFgBAKIAAAbIgHAAIAAg0IAGAAIABAHIABAAQACgEAFgCQAEgCAFAAQAKAAAEAEQAFAFABAKIAAAig");
	this.shape_7.setTransform(-8,-107.1);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgMAYQgGgDgDgGQgDgHAAgIQAAgMAHgHQAGgIALAAQALAAAHAIQAHAHAAAMQAAANgHAHQgHAIgLAAQgGAAgGgEgAgLgPQgEAGAAAJQAAAKAEAGQAEAFAHAAQAIAAAEgFQAEgGAAgKQAAgJgEgGQgEgFgIAAQgHAAgEAFg");
	this.shape_8.setTransform(-14.2,-107);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgSAbQgIgJAAgSQAAgKAFgIQAEgJAHgEQAIgFAKAAQALAAAIAEIgDAIQgJgEgHAAQgLAAgHAIQgHAHAAANQAAAOAHAIQAGAHAMAAQAHAAAKgCIAAAHQgIADgLAAQgPAAgJgKg");
	this.shape_9.setTransform(-20.2,-107.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol5, new cjs.Rectangle(-25.5,-117,53.9,17.6), null);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.arow();
	this.instance.parent = this;
	this.instance.setTransform(-14,-121,0.6,0.6);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol4, new cjs.Rectangle(-14,-121,9,9), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo();
	this.instance.parent = this;
	this.instance.setTransform(37,-186,0.75,0.75);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(37,-186,72,27.8), null);


(lib.bg_symbol = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.bg();
	this.instance.parent = this;
	this.instance.setTransform(18,-57,0.934,0.934);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.bg_symbol, new cjs.Rectangle(18,-57,280.4,233.6), null);


// stage content:
(lib.GSC1879_ABMBannerAd_300x50 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Your
	this.instance = new lib.Symbol10();
	this.instance.parent = this;
	this.instance.setTransform(-44.3,38.5,1,1,0,0,0,44.7,27.2);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(10).to({_off:false},0).wait(1).to({regX:4.7,regY:7,x:-56.4,y:18.2,alpha:0.25},0).wait(1).to({x:-28.5,y:18.1,alpha:0.5},0).wait(1).to({x:-0.6,y:17.9,alpha:0.75},0).wait(1).to({x:27.2,y:17.8,alpha:1},0).wait(116).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(24).to({regX:44.7,regY:27.2,x:-44.3,y:38.5},0).wait(1).to({regX:4.7,regY:7,x:-56.4,y:18.2,alpha:0.25},0).wait(1).to({x:-28.5,y:18.1,alpha:0.5},0).wait(1).to({x:-0.6,y:17.9,alpha:0.75},0).wait(1).to({x:27.2,y:17.8,alpha:1},0).wait(129));

	// Engineering 
	this.instance_1 = new lib.Symbol9();
	this.instance_1.parent = this;
	this.instance_1.setTransform(-100.7,72,1,1,0,0,0,116.2,27.2);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15).to({_off:false},0).wait(1).to({regX:75.4,regY:-27.1,x:-81.4,y:18.3,alpha:0.25},0).wait(1).to({x:-21.3,y:18.9,alpha:0.5},0).wait(1).to({x:38.8,y:19.4,alpha:0.75},0).wait(1).to({x:98.9,y:20,alpha:1},0).wait(111).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(5).to({_off:false,regX:116.2,regY:27.2,x:-100.7,y:72},0).wait(1).to({regX:75.4,regY:-27.1,x:-81.4,y:18.3,alpha:0.25},0).wait(1).to({x:-21.3,y:18.9,alpha:0.5},0).wait(1).to({x:38.8,y:19.4,alpha:0.75},0).wait(1).to({x:98.9,y:20,alpha:1},0).wait(124));

	// Co-Pilot
	this.instance_2 = new lib.Symbol8();
	this.instance_2.parent = this;
	this.instance_2.setTransform(-73.2,113.7,1,1,0,0,0,73.7,27.2);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(20).to({_off:false},0).wait(1).to({regX:300.3,regY:-68.4,x:160.6,y:18.1,alpha:0.25},0).wait(1).to({x:167.7,alpha:0.5},0).wait(1).to({x:174.9,alpha:0.75},0).wait(1).to({x:182,alpha:1},0).wait(106).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(10).to({_off:false,regX:73.7,regY:27.2,x:-73.2,y:113.7},0).wait(1).to({regX:300.3,regY:-68.4,x:160.6,y:18.1,alpha:0.25},0).wait(1).to({x:167.7,alpha:0.5},0).wait(1).to({x:174.9,alpha:0.75},0).wait(1).to({x:182,alpha:1},0).wait(119));

	// arrow_01
	this.instance_3 = new lib.Symbol4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(7.5,158.4,1,1,0,0,0,7.5,7.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(35).to({_off:false},0).wait(1).to({regX:-9.5,regY:-116.5,x:-1.5,y:34.5,alpha:0.333},0).wait(1).to({x:6.6,y:34.6,alpha:0.667},0).wait(1).to({x:14.6,y:34.7,alpha:1},0).wait(92).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(25).to({_off:false,regX:7.5,regY:7.5,x:7.5,y:158.4},0).wait(1).to({regX:-9.5,regY:-116.5,x:-1.5,y:34.5,alpha:0.333},0).wait(1).to({x:6.6,y:34.6,alpha:0.667},0).wait(1).to({x:14.6,y:34.7,alpha:1},0).wait(105));

	// Consulting
	this.instance_4 = new lib.Symbol5();
	this.instance_4.parent = this;
	this.instance_4.setTransform(104.3,158.2,1,1,0,0,0,51.7,15.6);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(35).to({_off:false},0).wait(1).to({regX:1.5,regY:-107,x:46.1,y:35.6,alpha:1},0).wait(94).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(25).to({_off:false,regX:51.7,regY:15.6,x:104.3,y:158.2},0).wait(1).to({regX:1.5,regY:-107,x:46.1,y:35.6,alpha:1},0).wait(107));

	// arrow_02
	this.instance_5 = new lib.Symbol4();
	this.instance_5.parent = this;
	this.instance_5.setTransform(90,158.6,1,1,0,0,0,7.5,7.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(45).to({_off:false},0).wait(1).to({regX:-9.5,regY:-116.5,x:77.7,y:34.6,alpha:0.333},0).wait(1).to({x:82.4,alpha:0.667},0).wait(1).to({x:87.1,y:34.7,alpha:1},0).wait(82).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(35).to({_off:false,regX:7.5,regY:7.5,x:90,y:158.6},0).wait(1).to({regX:-9.5,regY:-116.5,x:77.7,y:34.6,alpha:0.333},0).wait(1).to({x:82.4,alpha:0.667},0).wait(1).to({x:87.1,y:34.7,alpha:1},0).wait(95));

	// Software
	this.instance_6 = new lib.Symbol6();
	this.instance_6.parent = this;
	this.instance_6.setTransform(98,187,1,1,0,0,0,43.4,15.6);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(45).to({_off:false},0).wait(1).to({regX:69.7,regY:-137.1,x:114.3,y:34.8,alpha:1},0).wait(84).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(35).to({_off:false,regX:43.4,regY:15.6,x:98,y:187},0).wait(1).to({regX:69.7,regY:-137.1,x:114.3,y:34.8,alpha:1},0).wait(97));

	// arrow_03
	this.instance_7 = new lib.Symbol4();
	this.instance_7.parent = this;
	this.instance_7.setTransform(158.7,158.8,1,1,0,0,0,7.5,7.5);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(55).to({_off:false},0).wait(1).to({regX:-9.5,regY:-116.5,x:144.8,y:34.8,alpha:0.333},0).wait(1).to({x:148,y:34.7,alpha:0.667},0).wait(1).to({x:151.1,alpha:1},0).wait(72).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(45).to({_off:false,regX:7.5,regY:7.5,x:158.7,y:158.8},0).wait(1).to({regX:-9.5,regY:-116.5,x:144.8,y:34.8,alpha:0.333},0).wait(1).to({x:148,y:34.7,alpha:0.667},0).wait(1).to({x:151.1,alpha:1},0).wait(85));

	// Training
	this.instance_8 = new lib.Symbol7();
	this.instance_8.parent = this;
	this.instance_8.setTransform(92.8,216.9,1,1,0,0,0,39.4,15.6);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(55).to({_off:false},0).wait(1).to({regX:131.6,regY:-164.8,x:175.2,y:36.3,alpha:1},0).wait(74).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(45).to({_off:false,regX:39.4,regY:15.6,x:92.8,y:216.9},0).wait(1).to({regX:131.6,regY:-164.8,x:175.2,y:36.3,alpha:1},0).wait(87));

	// logo
	this.instance_9 = new lib.Symbol1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(235,215.8,1,1,0,0,0,48,18.5);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(20).to({_off:false},0).wait(1).to({regX:73,regY:-172.1,x:259.9,y:25.2,alpha:0.036},0).wait(1).to({x:259.8,alpha:0.071},0).wait(1).to({x:259.7,alpha:0.107},0).wait(1).to({x:259.5,y:25.3,alpha:0.143},0).wait(1).to({x:259.4,alpha:0.179},0).wait(1).to({x:259.3,alpha:0.214},0).wait(1).to({x:259.2,alpha:0.25},0).wait(1).to({x:259.1,y:25.4,alpha:0.286},0).wait(1).to({x:259,alpha:0.321},0).wait(1).to({x:258.8,alpha:0.357},0).wait(1).to({x:258.7,alpha:0.393},0).wait(1).to({x:258.6,y:25.5,alpha:0.429},0).wait(1).to({x:258.5,alpha:0.464},0).wait(1).to({x:258.4,alpha:0.5},0).wait(1).to({x:258.3,y:25.6,alpha:0.536},0).wait(1).to({x:258.1,alpha:0.571},0).wait(1).to({x:258,alpha:0.607},0).wait(1).to({x:257.9,alpha:0.643},0).wait(1).to({x:257.8,y:25.7,alpha:0.679},0).wait(1).to({x:257.7,alpha:0.714},0).wait(1).to({x:257.6,alpha:0.75},0).wait(1).to({x:257.4,alpha:0.786},0).wait(1).to({x:257.3,y:25.8,alpha:0.821},0).wait(1).to({x:257.2,alpha:0.857},0).wait(1).to({x:257.1,alpha:0.893},0).wait(1).to({x:257,alpha:0.929},0).wait(1).to({x:256.9,y:25.9,alpha:0.964},0).wait(1).to({x:256.8,alpha:1},0).wait(82).to({alpha:0.929},0).wait(1).to({alpha:0.857},0).wait(1).to({alpha:0.786},0).wait(1).to({alpha:0.714},0).wait(1).to({alpha:0.643},0).wait(1).to({alpha:0.571},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.429},0).wait(1).to({alpha:0.357},0).wait(1).to({alpha:0.286},0).wait(1).to({alpha:0.214},0).wait(1).to({alpha:0.143},0).wait(1).to({alpha:0.071},0).wait(1).to({alpha:0},0).wait(23).to({_off:true},1).wait(10).to({_off:false,regX:48,regY:18.5,x:235,y:215.8},0).wait(1).to({regX:73,regY:-172.1,x:259.9,y:25.2,alpha:0.036},0).wait(1).to({x:259.8,alpha:0.071},0).wait(1).to({x:259.7,alpha:0.107},0).wait(1).to({x:259.5,y:25.3,alpha:0.143},0).wait(1).to({x:259.4,alpha:0.179},0).wait(1).to({x:259.3,alpha:0.214},0).wait(1).to({x:259.2,alpha:0.25},0).wait(1).to({x:259.1,y:25.4,alpha:0.286},0).wait(1).to({x:259,alpha:0.321},0).wait(1).to({x:258.8,alpha:0.357},0).wait(1).to({x:258.7,alpha:0.393},0).wait(1).to({x:258.6,y:25.5,alpha:0.429},0).wait(1).to({x:258.5,alpha:0.464},0).wait(1).to({x:258.4,alpha:0.5},0).wait(1).to({x:258.3,y:25.6,alpha:0.536},0).wait(1).to({x:258.1,alpha:0.571},0).wait(1).to({x:258,alpha:0.607},0).wait(1).to({x:257.9,alpha:0.643},0).wait(1).to({x:257.8,y:25.7,alpha:0.679},0).wait(1).to({x:257.7,alpha:0.714},0).wait(1).to({x:257.6,alpha:0.75},0).wait(1).to({x:257.4,alpha:0.786},0).wait(1).to({x:257.3,y:25.8,alpha:0.821},0).wait(1).to({x:257.2,alpha:0.857},0).wait(1).to({x:257.1,alpha:0.893},0).wait(1).to({x:257,alpha:0.929},0).wait(1).to({x:256.9,y:25.9,alpha:0.964},0).wait(1).to({x:256.8,alpha:1},0).wait(95));

	// bg
	this.instance_10 = new lib.bg_symbol();
	this.instance_10.parent = this;
	this.instance_10.setTransform(102.6,125,1.264,1.264,0,0,0.3,139.9,117.8);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1).to({regX:158.2,regY:59.8,x:125.9,y:51.8},0).wait(1).to({x:126},0).wait(1).to({x:126.2},0).wait(1).to({x:126.4},0).wait(1).to({x:126.5},0).wait(1).to({x:126.7},0).wait(1).to({x:126.9},0).wait(1).to({x:127},0).wait(1).to({x:127.2,y:51.7},0).wait(1).to({x:127.4},0).wait(1).to({x:127.5},0).wait(1).to({x:127.7},0).wait(1).to({x:127.9},0).wait(1).to({x:128.1},0).wait(1).to({x:128.2},0).wait(1).to({x:128.4},0).wait(1).to({x:128.6},0).wait(1).to({x:128.7},0).wait(1).to({x:128.9},0).wait(1).to({x:129.1},0).wait(1).to({x:129.2,y:51.6},0).wait(1).to({x:129.4},0).wait(1).to({x:129.6},0).wait(1).to({x:129.7},0).wait(1).to({x:129.9},0).wait(1).to({x:130.1},0).wait(1).to({x:130.2},0).wait(1).to({x:130.4},0).wait(1).to({x:130.6},0).wait(1).to({x:130.7},0).wait(1).to({x:130.9},0).wait(1).to({x:131.1},0).wait(1).to({x:131.3,y:51.5},0).wait(1).to({x:131.4},0).wait(1).to({x:131.6},0).wait(1).to({x:131.8},0).wait(1).to({x:131.9},0).wait(1).to({x:132.1},0).wait(1).to({x:132.3},0).wait(1).to({x:132.4},0).wait(1).to({x:132.6},0).wait(1).to({x:132.8},0).wait(1).to({x:132.9},0).wait(1).to({x:133.1},0).wait(1).to({x:133.3,y:51.4},0).wait(1).to({x:133.4},0).wait(1).to({x:133.6},0).wait(1).to({x:133.8},0).wait(1).to({x:133.9},0).wait(1).to({x:134.1},0).wait(1).to({x:134.3},0).wait(1).to({x:134.5},0).wait(1).to({x:134.6},0).wait(1).to({x:134.8},0).wait(1).to({x:135},0).wait(1).to({x:135.1},0).wait(1).to({x:135.3,y:51.3},0).wait(1).to({x:135.5},0).wait(1).to({x:135.6},0).wait(1).to({x:135.8},0).wait(1).to({x:136},0).wait(1).to({x:136.1},0).wait(1).to({x:136.3},0).wait(1).to({x:136.5},0).wait(1).to({x:136.6},0).wait(1).to({x:136.8},0).wait(1).to({x:137},0).wait(1).to({x:137.1},0).wait(1).to({x:137.3,y:51.2},0).wait(1).to({x:137.5},0).wait(1).to({x:137.7},0).wait(1).to({x:137.8},0).wait(1).to({x:138},0).wait(1).to({x:138.2},0).wait(1).to({x:138.3},0).wait(1).to({x:138.5},0).wait(1).to({x:138.7},0).wait(1).to({x:138.8},0).wait(1).to({x:139},0).wait(1).to({x:139.2},0).wait(1).to({x:139.3,y:51.1},0).wait(1).to({x:139.5},0).wait(1).to({x:139.7},0).wait(1).to({x:139.8},0).wait(1).to({x:140},0).wait(1).to({x:140.2},0).wait(1).to({x:140.3},0).wait(1).to({x:140.5},0).wait(1).to({x:140.7},0).wait(1).to({x:140.9},0).wait(1).to({x:141},0).wait(1).to({x:141.2},0).wait(1).to({x:141.4,y:51},0).wait(1).to({x:141.5},0).wait(1).to({x:141.7},0).wait(1).to({x:141.9},0).wait(1).to({x:142},0).wait(1).to({x:142.2},0).wait(1).to({x:142.4},0).wait(1).to({x:142.5},0).wait(1).to({x:142.7},0).wait(1).to({x:142.9},0).wait(1).to({x:143},0).wait(1).to({x:143.2},0).wait(1).to({x:143.4,y:50.9},0).wait(1).to({x:143.5},0).wait(1).to({x:143.7},0).wait(1).to({x:143.9},0).wait(1).to({x:144.1},0).wait(1).to({x:144.2},0).wait(1).to({x:144.4},0).wait(1).to({x:144.6},0).wait(1).to({x:144.7},0).wait(1).to({x:144.9},0).wait(1).to({x:145.1},0).wait(1).to({x:145.2},0).wait(1).to({x:145.4,y:50.8},0).wait(1).to({x:145.6},0).wait(1).to({x:145.7},0).wait(1).to({x:145.9},0).wait(1).to({x:146.1},0).wait(1).to({x:146.2},0).wait(1).to({x:146.4},0).wait(1).to({x:146.6},0).wait(1).to({x:146.7},0).wait(1).to({x:146.9},0).wait(1).to({x:147.1},0).wait(1).to({x:147.3},0).wait(1).to({x:147.4,y:50.7},0).wait(1).to({x:147.6},0).wait(1).to({x:147.8},0).wait(1).to({x:147.9},0).wait(1).to({x:148.1},0).wait(1).to({x:148.3},0).wait(1).to({x:148.4},0).wait(1).to({x:148.6},0).wait(1).to({x:148.8},0).wait(1).to({x:148.9},0).wait(1).to({x:149.1},0).wait(1).to({x:149.3},0).wait(1).to({x:149.4,y:50.6},0).wait(1).to({x:149.6},0).wait(1).to({x:149.8},0).wait(1).to({x:149.9},0).wait(1).to({x:150.1},0).wait(1).to({x:150.3},0).wait(1).to({x:150.4},0).wait(1).to({x:150.6},0).wait(1).to({x:150.8},0).wait(1).to({x:151},0).wait(1).to({x:151.1},0).wait(1).to({x:151.3},0).wait(1).to({x:151.5,y:50.5},0).wait(1).to({x:151.6},0).wait(1).to({x:151.8},0).wait(1).to({x:152},0).wait(1).to({x:152.1},0).wait(1).to({x:152.3},0).wait(1).to({x:152.5},0).wait(1).to({x:152.6},0).wait(1).to({x:152.8},0).wait(1).to({x:153},0).wait(1).to({x:153.1},0).wait(1).to({x:153.3},0).wait(1).to({x:153.5,y:50.4},0).wait(1).to({x:153.6},0).wait(1).to({x:153.8},0).wait(1).to({x:154},0).wait(1).to({x:154.2},0).wait(1).to({x:154.3},0).wait(1).to({x:154.5},0).wait(1).to({x:154.7},0).wait(1).to({x:154.8},0).wait(1).to({x:155},0).wait(1).to({x:155.2},0).wait(1).to({x:155.3},0).wait(1).to({x:155.5,y:50.3},0).wait(1).to({x:155.7},0).wait(1).to({x:155.8},0).wait(1).to({x:156},0).wait(1).to({x:156.2},0).wait(1).to({x:156.3},0).wait(1).to({x:156.5},0).wait(1).to({x:156.7},0).wait(1).to({x:156.8},0).wait(1).to({x:157},0).wait(1).to({x:157.2},0).wait(1).to({x:157.4},0).wait(1).to({x:157.5,y:50.2},0).wait(1).to({x:157.7},0).wait(1).to({x:157.9},0).wait(1).to({x:158},0).wait(1).to({x:158.2},0).wait(1).to({x:158.4},0).wait(1).to({x:158.5},0).wait(1).to({x:158.7},0).wait(1).to({x:158.9},0).wait(1).to({x:159},0).wait(1).to({x:159.2},0).wait(1).to({x:159.4},0).wait(1).to({x:159.5,y:50.1},0).wait(1).to({x:159.7},0).wait(1).to({x:159.9},0).wait(1).to({x:160},0).wait(1).to({x:160.2},0).wait(1).to({x:160.4},0).wait(1).to({x:160.6},0).wait(1).to({x:160.7},0).wait(1).to({x:160.9},0).wait(1).to({x:161.1},0).wait(1).to({x:161.2},0).wait(1).to({x:161.4},0).wait(1).to({x:161.6,y:50},0).wait(1).to({x:161.7},0).wait(1).to({x:161.9},0).wait(1).to({x:162.1},0).wait(1).to({x:162.2},0).wait(1).to({x:162.4},0).wait(1).to({x:162.6},0).wait(1).to({x:162.7},0).wait(1).to({x:162.9},0).wait(1).to({x:163.1},0).wait(1).to({x:163.2},0).wait(1).to({x:163.4},0).wait(1).to({x:163.6,y:49.9},0).wait(1).to({x:163.8},0).wait(1).to({x:163.9},0).wait(1).to({x:164.1},0).wait(1).to({x:164.3},0).wait(1).to({x:164.4},0).wait(1).to({x:164.6},0).wait(1).to({x:164.8},0).wait(1).to({x:164.9},0).wait(1).to({x:165.1},0).wait(1).to({x:165.3},0).wait(1).to({x:165.4},0).wait(1).to({x:165.6,y:49.8},0).wait(1).to({x:165.8},0).wait(1).to({x:165.9},0).wait(1).to({x:166.1},0).wait(1).to({x:166.3},0).wait(1).to({x:166.4},0).wait(1).to({x:166.6},0).wait(1).to({x:166.8},0).wait(1).to({x:167},0).wait(1).to({x:167.1},0).wait(1).to({x:167.3},0).wait(1).to({x:167.5},0).wait(1).to({x:167.6,y:49.7},0).wait(1).to({x:167.8},0).wait(1).to({x:168},0).wait(1).to({x:168.1},0).wait(1).to({x:168.3},0).wait(1).to({x:168.5},0).wait(1).to({x:168.6},0).wait(1).to({x:168.8},0).wait(1).to({x:169},0).wait(1).to({x:169.1},0).wait(1).to({x:169.3},0).wait(1).to({x:169.5},0).wait(1).to({x:169.6,y:49.6},0).wait(1).to({x:169.8},0).wait(1).to({x:170},0).wait(1).to({x:170.2},0).wait(1).to({x:170.3},0).wait(1).to({x:170.5},0).wait(1).to({x:170.7},0).wait(1).to({x:170.8},0).wait(1).to({x:171},0).wait(1).to({x:171.2},0).wait(1).to({x:171.3},0).wait(1).to({x:171.5},0).wait(1).to({x:171.7,y:49.5},0).wait(1).to({x:171.8},0).wait(1).to({x:172},0).wait(1).to({x:172.2},0).wait(1).to({x:172.3},0).wait(1).to({x:172.5},0).wait(1).to({x:172.7},0).wait(1).to({x:172.8},0).wait(1).to({x:173},0).wait(1).to({x:173.2},0).wait(1).to({x:173.4},0).wait(1).to({x:173.5},0).wait(1).to({x:173.7,y:49.4},0).wait(1).to({x:173.9},0).wait(1).to({x:174},0).wait(1).to({x:174.2},0).wait(1).to({x:174.4},0).wait(1).to({x:174.5},0).wait(1).to({x:174.7},0).wait(1).to({x:174.9},0).wait(1).to({x:175},0).wait(1).to({x:175.2},0).wait(1).to({x:175.4},0).wait(1).to({x:175.5,y:49.3},0).wait(1).to({x:175.7},0).wait(1).to({x:175.9},0).wait(1).to({x:176},0).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(98.4,-71.7,354.3,296.9);
// library properties:
lib.properties = {
	id: 'D4C83844DFB7CD4CA2819D590FAC090D',
	width: 300,
	height: 50,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/GSC1879_ABM_Banner_Ad_300x50_atlas_.png", id:"GSC1879_ABM_Banner_Ad_300x50_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D4C83844DFB7CD4CA2819D590FAC090D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;