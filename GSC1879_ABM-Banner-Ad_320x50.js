(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [
		{name:"GSC1879_ABM_Banner_Ad_320x50_atlas_", frames: [[0,0,320,50],[322,0,175,67]]}
];


// symbols:



(lib.GSC1879_ABMBannerAd_320x50_bg = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_320x50_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.logo_big = function() {
	this.spriteSheet = ss["GSC1879_ABM_Banner_Ad_320x50_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.Your = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgdAvIAAhcIATAAIAEAQIABAAQAFgIAFgEQAIgFAJAAIAIAAIgCAYIgHgBQgMAAgHAHQgGAGAAAKIAAAvg");
	this.shape.setTransform(37.8,15.4);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgiAnQgIgJAAgRIAAg7IAZAAIAAA1QAAAKAEAFQADAFAIAAQAKAAAFgHQAEgHAAgPIAAgsIAaAAIAABcIgUAAIgDgMIgBAAQgFAGgHAEQgHADgJAAQgQAAgJgIg");
	this.shape_1.setTransform(27.9,15.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgWAqQgLgGgFgKQgGgMAAgOQAAgWAMgMQAMgNAUAAQANAAALAGQAKAFAGAMQAFALAAANQAAAXgMANQgMAMgVAAQgMAAgKgGgAgNgUQgFAHAAANQAAANAFAIQAEAHAJAAQAKAAAFgHQAEgHAAgOQAAgNgFgHQgDgGgLAAQgJAAgEAGg");
	this.shape_2.setTransform(17,15.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgMA9IAAgvIgohKIAcAAIAYAyIAZgyIAcAAIgoBKIAAAvg");
	this.shape_3.setTransform(7.3,14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Your, new cjs.Rectangle(0,0,43.4,27.2), null);


(lib.Training = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgXAqQgGgEAAgJQAAgGADgEQAEgEAHgCQgDgBgBgCQgCgDAAgDQAAgDACgCIAGgFQgFgCgDgFQgDgFAAgGQAAgLAGgFQAGgGAMAAQAEAAAEABIAWAAIAAAGIgMACIADAFIABAHQAAAKgGAFQgGAGgLAAIgFAAQgGADAAAEQAAABAAAAQAAABAAAAQAAABABAAQAAAAABABQACABAFAAIALAAQAKAAAGAEQAFAFAAAIQAAALgIAGQgJAFgPAAQgNAAgHgFgAgQAUQgEADAAAGQAAAFAEADQAFADAIAAQALAAAGgEQAGgEAAgGQAAgFgDgCQgEgCgIAAIgLAAQgHAAgDADgAgLgjQgEADAAAIQAAAGAEAEQAEADAHAAQANAAAAgOQAAgOgOAAQgGAAgEAEg");
	this.shape.setTransform(88.2,13);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AARAhIAAgpQAAgIgEgEQgDgDgIAAQgJAAgFAFQgEAGAAAMIAAAhIgKAAIAAg/IAIAAIACAIIAAAAQADgEAFgDQAGgDAFAAQAMAAAGAGQAGAGAAAMIAAApg");
	this.shape_1.setTransform(81.3,11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEAsIAAg/IAJAAIAAA/gAgDggQgCgBAAgEQAAgDACgBIADgCQAAAAABAAQABAAAAAAQABABAAAAQABAAAAABQACABAAADQAAAEgCABQAAABgBAAQAAAAgBABQAAAAgBAAQgBAAAAAAIgDgCg");
	this.shape_2.setTransform(76,10.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AARAhIAAgpQAAgIgEgEQgDgDgIAAQgJAAgFAFQgEAGAAAMIAAAhIgKAAIAAg/IAIAAIACAIIAAAAQADgEAFgDQAGgDAFAAQAMAAAGAGQAGAGAAAMIAAApg");
	this.shape_3.setTransform(70.9,11.6);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgEAsIAAg/IAJAAIAAA/gAgDggQgCgBAAgEQAAgDACgBIADgCQAAAAABAAQABAAAAAAQABABAAAAQABAAAAABQACABAAADQAAAEgCABQAAABgBAAQAAAAgBABQAAAAgBAAQgBAAAAAAIgDgCg");
	this.shape_4.setTransform(65.6,10.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgTAdQgGgFAAgKQAAgSAegBIALAAIAAgEQAAgIgDgDQgDgEgHAAQgHAAgLAFIgDgIIALgEIAKgBQAMAAAFAFQAGAFAAALIAAArIgHAAIgCgJIgBAAQgFAGgEACQgFACgGAAQgKAAgFgEgAAGABQgKAAgFAEQgGADAAAHQAAAFAEADQADACAGAAQAIAAAFgFQAFgFAAgJIAAgFg");
	this.shape_5.setTransform(60.6,11.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgSAhIAAg/IAIAAIABALIABAAQADgGAFgDQAFgEAGAAIAHABIgBAJIgHgBQgIAAgFAHQgFAGAAAJIAAAig");
	this.shape_6.setTransform(55.7,11.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgEArIAAhMIgbAAIAAgJIA/AAIAAAJIgbAAIAABMg");
	this.shape_7.setTransform(50.1,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Training, new cjs.Rectangle(44.7,0,48.9,20.4), null);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.logo_big();
	this.instance.parent = this;
	this.instance.setTransform(211,-40,0.399,0.399);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.Symbol1, new cjs.Rectangle(211,-40,69.9,26.8), null);


(lib.Software = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgSAZQgIgJAAgPQAAgPAIgJQAHgJAMAAQAMAAAHAIQAHAIAAANIAAAFIgrAAQAAALAGAGQAFAGAJAAQAKAAALgEIAAAIIgKADIgLABQgOAAgIgIgAgKgTQgFAFgBAJIAhAAQAAgJgEgFQgEgFgIAAQgHAAgEAFg");
	this.shape.setTransform(97.8,11.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgRAhIAAg/IAIAAIABALIAAAAQAEgGAEgDQAFgEAFAAIAIABIgBAJIgHgBQgIAAgEAHQgGAGAAAJIAAAig");
	this.shape_1.setTransform(92.7,11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgTAdQgGgFAAgKQAAgSAegBIALAAIAAgEQAAgIgDgDQgDgEgHAAQgHAAgLAFIgDgIIALgEIAKgBQAMAAAFAFQAGAFAAALIAAArIgHAAIgCgJIgBAAQgFAGgEACQgFACgGAAQgKAAgFgEgAAGABQgKAAgFAEQgGADAAAHQAAAFAEADQADACAGAAQAIAAAFgFQAFgFAAgJIAAgFg");
	this.shape_2.setTransform(86.2,11.6);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AAQAgIgMglIgEgPIAAAAIgEAPIgMAlIgLAAIgRg/IAKAAIAJAjQAEANAAAEIAAAAIADgIIACgJIAMgjIAJAAIAMAjQADALABAGIABAAIABgGIAMguIAKAAIgRA/g");
	this.shape_3.setTransform(78.4,11.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgJAUIAAglIgJAAIAAgEIAJgFIAEgNIAFAAIAAAPIASAAIAAAHIgSAAIAAAlQAAAGACADQAEADAEAAIAFAAIAEgBIAAAHIgFACIgGAAQgRAAAAgUg");
	this.shape_4.setTransform(71.6,11);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgKAuIAAg4IgLAAIAAgEIALgEIAAgDQAAgYAUAAQAFAAAHACIgCAIQgGgCgEAAQgGAAgCAEQgCADAAAJIAAAEIAQAAIAAAHIgQAAIAAA4g");
	this.shape_5.setTransform(67.8,10.3);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgPAdQgGgEgEgHQgDgIAAgKQgBgPAJgIQAHgJANAAQAOAAAIAJQAIAJgBAOQABAQgIAJQgIAIgOAAQgHAAgIgEgAgNgSQgFAHAAALQAAAMAFAHQAEAGAJAAQAKAAAFgGQAEgHAAgMQAAgLgEgGQgFgHgKAAQgJAAgEAGg");
	this.shape_6.setTransform(61.8,11.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgaAoIAAgKQAFADAHABIAMACQAJAAAFgFQAFgDAAgHQAAgFgCgCQgCgDgDgCIgNgGQgLgDgGgGQgEgGAAgKQgBgJAIgGQAHgGAMAAQAMAAALAFIgDAIQgLgEgKAAQgGAAgFADQgFAEABAGQgBAEACADQACADAEADIALAEQANAEAFAGQAFAFAAAJQAAAMgIAGQgJAGgNAAQgOAAgJgEg");
	this.shape_7.setTransform(54.9,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Software, new cjs.Rectangle(49.6,0,53.7,20.4), null);


(lib.Engineering = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgjA9QgLgHAAgMQAAgJAFgFQAGgGAKgCQgEgCgDgEQgDgDAAgFQAAgFADgCQADgEAGgDQgHgEgEgHQgFgHAAgJQAAgPAKgIQAKgJASAAIAJABIAGABIAhAAIAAANIgPAEQAEAGAAAHQAAAPgKAJQgLAIgRAAIgFAAIgDgBQgEADAAADQAAAGAOAAIAOAAQAQAAAIAGQAIAHAAAMQAAARgOAJQgNAJgZAAQgTAAgKgHgAgTAfQgFADAAAGQAAAFAFADQAFADAJAAQANAAAIgEQAHgDAAgHQAAgFgEgCQgFgCgJAAIgMAAQgHAAgFADgAgPgjQAAAIAEAEQADAFAHAAQAGAAAEgFQADgEAAgIQAAgQgNAAQgOAAAAAQg");
	this.shape.setTransform(97.5,17.5);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AARAvIAAg1QAAgKgDgFQgEgFgIAAQgJAAgFAHQgFAHAAAQIAAArIgZAAIAAhcIATAAIAEAMIABAAQAEgGAIgEQAGgDAKAAQAQAAAIAJQAJAIAAARIAAA7g");
	this.shape_1.setTransform(87.2,15.4);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgLBBIAAhcIAYAAIAABcgAgNgzQAAgMANAAQAOAAAAAMQAAAFgEAEQgDADgHAAQgNAAAAgMg");
	this.shape_2.setTransform(79,13.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgdAvIAAhcIATAAIADAQIACAAQAEgIAHgEQAHgFAJAAIAIAAIgCAYIgIgBQgLAAgHAHQgGAGAAAKIAAAvg");
	this.shape_3.setTransform(73.1,15.4);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgdAkQgNgNAAgWQAAgWAMgNQAMgNATAAQAUAAALALQALALAAAUIAAALIg7AAQAAALAGAGQAGAHAKgBQAJAAAHgBQAIgCAIgEIAAATQgHAEgIACQgHABgLAAQgVAAgNgMgAgKgYQgFAFAAAKIAiAAQAAgKgFgFQgEgFgJAAQgHAAgEAFg");
	this.shape_4.setTransform(63.7,15.5);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgdAkQgNgNAAgWQAAgWAMgNQAMgNATAAQAUAAALALQALALAAAUIAAALIg7AAQAAALAGAGQAGAHAKgBQAJAAAHgBQAIgCAIgEIAAATQgHAEgIACQgHABgLAAQgVAAgNgMgAgKgYQgFAFAAAKIAiAAQAAgKgFgFQgEgFgJAAQgHAAgEAFg");
	this.shape_5.setTransform(53.6,15.5);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AARAvIAAg1QAAgKgDgFQgEgFgIAAQgJAAgFAHQgFAHAAAQIAAArIgZAAIAAhcIATAAIAEAMIABAAQAEgGAIgEQAGgDAKAAQAQAAAIAJQAJAIAAARIAAA7g");
	this.shape_6.setTransform(43.1,15.4);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgMBBIAAhcIAYAAIAABcgAgNgzQAAgMANAAQAOAAAAAMQAAAFgDAEQgEADgHAAQgNAAAAgMg");
	this.shape_7.setTransform(34.9,13.7);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgjA9QgLgHAAgMQAAgJAFgFQAGgGAKgCQgEgCgDgEQgDgDAAgFQAAgFADgCQADgEAGgDQgHgEgEgHQgFgHAAgJQAAgPAKgIQAKgJASAAIAJABIAGABIAhAAIAAANIgPAEQAEAGAAAHQAAAPgKAJQgLAIgRAAIgFAAIgDgBQgEADAAADQAAAGAOAAIAOAAQAQAAAIAGQAIAHAAAMQAAARgOAJQgNAJgZAAQgTAAgKgHgAgTAfQgFADAAAGQAAAFAFADQAFADAJAAQANAAAIgEQAHgDAAgHQAAgFgEgCQgFgCgJAAIgMAAQgHAAgFADgAgPgjQAAAIAEAEQADAFAHAAQAGAAAEgFQADgEAAgIQAAgQgNAAQgOAAAAAQg");
	this.shape_8.setTransform(27.4,17.5);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AARAvIAAg1QAAgKgDgFQgEgFgIAAQgJAAgFAHQgFAHAAAQIAAArIgZAAIAAhcIATAAIAEAMIABAAQAEgGAIgEQAGgDAKAAQAQAAAIAJQAJAIAAARIAAA7g");
	this.shape_9.setTransform(17.1,15.4);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#FFFFFF").s().p("AgiA9IAAh5IBFAAIAAAVIgrAAIAAAbIAoAAIAAAUIgoAAIAAAgIArAAIAAAVg");
	this.shape_10.setTransform(7,14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_10},{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Engineering, new cjs.Rectangle(0,0,104.6,27.2), null);


(lib.Consulting = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgXAqQgGgEAAgJQAAgGADgEQAEgEAHgCQgDgBgBgCQgCgDAAgDQAAgDACgCIAGgFQgFgCgDgFQgDgFAAgGQAAgLAGgFQAGgGAMAAQAEAAAEABIAWAAIAAAGIgMACIADAFIABAHQAAAKgGAFQgGAGgLAAIgFAAQgGADAAAEQAAABAAAAQAAABAAAAQABABAAAAQAAAAABABQACABAFAAIALAAQAKAAAGAEQAFAFAAAIQAAALgIAGQgJAFgPAAQgNAAgHgFgAgQAUQgEADAAAGQAAAFAEADQAFADAIAAQALAAAGgEQAGgEAAgGQAAgFgDgCQgEgCgIAAIgLAAQgHAAgDADgAgLgjQgEADAAAIQAAAGAEAEQAEADAHAAQANAAAAgOQAAgOgOAAQgGAAgEAEg");
	this.shape.setTransform(117.7,13);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AARAhIAAgpQAAgIgEgEQgDgDgIAAQgJAAgFAFQgEAGAAAMIAAAhIgKAAIAAg/IAIAAIACAIIAAAAQADgEAFgDQAGgDAFAAQAMAAAGAGQAGAGAAAMIAAApg");
	this.shape_1.setTransform(110.8,11.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgEAsIAAg/IAJAAIAAA/gAgDggQgCgBAAgEQAAgDACgBIADgCQAAAAABAAQABAAAAAAQABABAAAAQABAAAAABQACABAAADQAAAEgCABQAAABgBAAQAAAAgBABQAAAAgBAAQgBAAAAAAIgDgCg");
	this.shape_2.setTransform(105.5,10.5);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgJAUIAAglIgJAAIAAgEIAJgFIAEgNIAFAAIAAAPIASAAIAAAHIgSAAIAAAlQAAAGACADQADADAFAAIAFAAIAEgBIAAAHIgFACIgGAAQgRAAAAgUg");
	this.shape_3.setTransform(101.9,11);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgEAtIAAhZIAJAAIAABZg");
	this.shape_4.setTransform(98.2,10.3);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgUAbQgGgGAAgLIAAgpIAKAAIAAAoQAAAIADAEQAEADAIAAQAJABAEgGQAFgFAAgNIAAggIAKAAIAAA+IgIAAIgBgIIgBAAQgDAEgFADQgGADgGgBQgLAAgGgFg");
	this.shape_5.setTransform(93,11.7);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgVAdIAAgJIAKAEIAKABQAGAAAFgCQAEgCAAgFQAAgEgEgDQgDgDgIgDIgNgFQgEgCgCgEQgCgDAAgEQAAgIAHgFQAGgEAKAAQALAAAKAEIgEAIQgJgEgIAAQgGAAgEACQgDACAAAEQAAABAAAAQAAABAAABQAAAAABABQAAAAAAABIAEADIALAFQALADAEAEQAEAEAAAHQAAAJgHAFQgGAEgLAAQgNAAgHgEg");
	this.shape_6.setTransform(86.5,11.6);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AARAhIAAgpQAAgIgEgEQgDgDgIAAQgJAAgFAFQgEAGAAAMIAAAhIgKAAIAAg/IAIAAIACAIIAAAAQADgEAFgDQAGgDAFAAQAMAAAGAGQAGAGAAAMIAAApg");
	this.shape_7.setTransform(80,11.6);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFFFFF").s().p("AgPAdQgGgEgDgHQgEgIgBgKQABgPAHgIQAJgJANAAQANAAAIAJQAHAJAAAOQAAAQgHAJQgIAIgOAAQgHAAgIgEgAgOgSQgEAHAAALQAAAMAEAHQAGAGAIAAQAKAAAEgGQAGgHAAgMQAAgLgGgGQgEgHgKAAQgIAAgGAGg");
	this.shape_8.setTransform(72.7,11.6);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FFFFFF").s().p("AgVAgQgKgLAAgVQAAgMAFgKQAEgKAKgGQAJgFAMAAQANAAAKAFIgEAIQgKgEgJAAQgNAAgJAKQgIAJAAAPQAAARAIAJQAIAKAOAAQAJAAALgEIAAAJQgJADgNAAQgSAAgKgMg");
	this.shape_9.setTransform(65.5,10.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_9},{t:this.shape_8},{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.Consulting, new cjs.Rectangle(59.5,0,63.6,20.4), null);


(lib.CoPilot = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("AgMAxQgHgHAAgPIAAgsIgMAAIAAgLIAOgIIAHgUIAPAAIAAAUIAaAAIAAATIgaAAIAAAsQAAAFADADQADACAFAAQAHAAAJgDIAAATQgJAEgOAAQgOAAgHgIg");
	this.shape.setTransform(64,14.6);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#FFFFFF").s().p("AgXAqQgKgGgGgKQgFgMAAgOQAAgWAMgMQAMgNAVAAQANAAAKAGQAKAFAFAMQAGALAAANQAAAXgMANQgMAMgVAAQgMAAgLgGgAgOgUQgEAHAAANQAAANAEAIQAFAHAJAAQALAAAEgHQAEgHAAgOQAAgNgFgHQgDgGgLAAQgJAAgFAGg");
	this.shape_1.setTransform(55.1,15.5);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#FFFFFF").s().p("AgMBBIAAiAIAZAAIAACAg");
	this.shape_2.setTransform(47.3,13.7);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("AgMBBIAAhcIAYAAIAABcgAgNgzQAAgMANAAQAOAAAAAMQAAAFgDAEQgEADgHAAQgNAAAAgMg");
	this.shape_3.setTransform(42.1,13.7);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FFFFFF").s().p("AgpA9IAAh5IAnAAQAVAAALAKQAMAJAAATQAAAUgMAJQgNALgVAAIgLAAIAAArgAgPgCIAIAAQALAAAGgFQAGgFAAgJQAAgJgFgEQgFgFgKAAIgLAAg");
	this.shape_4.setTransform(34.5,14.1);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FFFFFF").s().p("AgVAKIAAgTIArAAIAAATg");
	this.shape_5.setTransform(26.1,15.6);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#FFFFFF").s().p("AgXAqQgKgGgFgKQgGgMAAgOQAAgWAMgMQAMgNAUAAQANAAALAGQAKAFAFAMQAGALAAANQAAAXgMANQgMAMgVAAQgMAAgLgGgAgOgUQgEAHAAANQAAANAEAIQAFAHAJAAQAKAAAFgHQAEgHAAgOQAAgNgEgHQgFgGgKAAQgJAAgFAGg");
	this.shape_6.setTransform(18.1,15.5);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#FFFFFF").s().p("AgfAuQgOgQAAgeQAAgSAHgOQAGgOANgIQANgHARAAQARAAASAIIgIAVIgOgGQgHgCgGAAQgOAAgIALQgIALAAASQAAApAeAAQAMAAATgGIAAAVQgPAGgTAAQgZAAgOgQg");
	this.shape_7.setTransform(7.6,14.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = getMCSymbolPrototype(lib.CoPilot, new cjs.Rectangle(0,0,70,27.2), null);


(lib.arrow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#53BCEB").s().p("AgFAmIAXgmIgXgmIAYAAIAWAnIgVAlgAgoAmIAXgmIgXgmIAZAAIAVAnIgUAlg");
	this.shape.setTransform(4.1,3.4);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = getMCSymbolPrototype(lib.arrow, new cjs.Rectangle(0,-0.5,8.2,7.7), null);


// stage content:
(lib.GSC1879_ABMBannerAd_320x50 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// Training
	this.instance = new lib.Training();
	this.instance.parent = this;
	this.instance.setTransform(199.2,50,1,1,0,0,0,69.1,26.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(37).to({_off:false},0).wait(1).to({regX:69,regY:11.9,x:195.5,y:35.4,alpha:0.25},0).wait(1).to({x:191.9,alpha:0.5},0).wait(1).to({x:188.2,alpha:0.75},0).wait(1).to({x:184.6,alpha:1},0).wait(28).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(37).to({_off:false,regX:69.1,regY:26.5,x:199.2,y:50},0).wait(1).to({regX:69,regY:11.9,x:195.5,y:35.4,alpha:0.25},0).wait(1).to({x:191.9,alpha:0.5},0).wait(1).to({x:188.2,alpha:0.75},0).wait(1).to({x:184.6,alpha:1},0).wait(21));

	// Arrow 3
	this.instance_1 = new lib.arrow();
	this.instance_1.parent = this;
	this.instance_1.setTransform(140.1,43.9,1,1,0,0,0,13.2,12.6);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(37).to({_off:false},0).wait(1).to({regX:4.1,regY:3.4,x:137.2,y:34.7,alpha:0.25},0).wait(1).to({x:143.5,alpha:0.5},0).wait(1).to({x:149.7,alpha:0.75},0).wait(1).to({x:156,alpha:1},0).wait(28).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(37).to({_off:false,regX:13.2,regY:12.6,x:140.1,y:43.9},0).wait(1).to({regX:4.1,regY:3.4,x:137.2,y:34.7,alpha:0.25},0).wait(1).to({x:143.5,alpha:0.5},0).wait(1).to({x:149.7,alpha:0.75},0).wait(1).to({x:156,alpha:1},0).wait(21));

	// Software
	this.instance_2 = new lib.Software();
	this.instance_2.parent = this;
	this.instance_2.setTransform(134.4,50,1,1,0,0,0,76.4,26.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(33).to({_off:false},0).wait(1).to({regX:76.3,regY:10.3,x:131.3,y:33.8,alpha:0.25},0).wait(1).to({x:128.3,alpha:0.5},0).wait(1).to({x:125.3,alpha:0.75},0).wait(1).to({x:122.3,alpha:1},0).wait(32).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(33).to({_off:false,regX:76.4,regY:26.5,x:134.4,y:50},0).wait(1).to({regX:76.3,regY:10.3,x:131.3,y:33.8,alpha:0.25},0).wait(1).to({x:128.3,alpha:0.5},0).wait(1).to({x:125.3,alpha:0.75},0).wait(1).to({x:122.3,alpha:1},0).wait(25));

	// Arrow 2
	this.instance_3 = new lib.arrow();
	this.instance_3.parent = this;
	this.instance_3.setTransform(74.1,43.9,1,1,0,0,0,13.2,12.6);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(33).to({_off:false},0).wait(1).to({regX:4.1,regY:3.4,x:71.5,y:34.7,alpha:0.25},0).wait(1).to({x:78,alpha:0.5},0).wait(1).to({x:84.5,alpha:0.75},0).wait(1).to({x:91,alpha:1},0).wait(32).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(33).to({_off:false,regX:13.2,regY:12.6,x:74.1,y:43.9},0).wait(1).to({regX:4.1,regY:3.4,x:71.5,y:34.7,alpha:0.25},0).wait(1).to({x:78,alpha:0.5},0).wait(1).to({x:84.5,alpha:0.75},0).wait(1).to({x:91,alpha:1},0).wait(25));

	// Consulting
	this.instance_4 = new lib.Consulting();
	this.instance_4.parent = this;
	this.instance_4.setTransform(65.8,50,1,1,0,0,0,91.3,26.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(29).to({_off:false},0).wait(1).to({regX:91.5,regY:11.8,x:62.8,y:35.3,alpha:0.25},0).wait(1).to({x:59.5,alpha:0.5},0).wait(1).to({x:56.3,alpha:0.75},0).wait(1).to({x:53,alpha:1},0).wait(36).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(29).to({_off:false,regX:91.3,regY:26.5,x:65.8,y:50},0).wait(1).to({regX:91.5,regY:11.8,x:62.8,y:35.3,alpha:0.25},0).wait(1).to({x:59.5,alpha:0.5},0).wait(1).to({x:56.3,alpha:0.75},0).wait(1).to({x:53,alpha:1},0).wait(29));

	// Arrow 1
	this.instance_5 = new lib.arrow();
	this.instance_5.parent = this;
	this.instance_5.setTransform(1.1,43.9,1,1,0,0,0,13.2,12.6);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(29).to({_off:false},0).wait(1).to({regX:4.1,regY:3.4,x:-1.8,y:34.7,alpha:0.25},0).wait(1).to({x:4.5,alpha:0.5},0).wait(1).to({x:10.7,alpha:0.75},0).wait(1).to({x:17,alpha:1},0).wait(36).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(29).to({_off:false,regX:13.2,regY:12.6,x:1.1,y:43.9},0).wait(1).to({regX:4.1,regY:3.4,x:-1.8,y:34.7,alpha:0.25},0).wait(1).to({x:4.5,alpha:0.5},0).wait(1).to({x:10.7,alpha:0.75},0).wait(1).to({x:17,alpha:1},0).wait(29));

	// Co-Pilot
	this.instance_6 = new lib.CoPilot();
	this.instance_6.parent = this;
	this.instance_6.setTransform(265.3,35.3,1,1,0,0,0,87.3,31.9);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(17).to({_off:false},0).wait(1).to({regX:35.1,regY:13.8,x:207.9,y:17.2,alpha:0.25},0).wait(1).to({x:202.8,alpha:0.5},0).wait(1).to({x:197.7,alpha:0.75},0).wait(1).to({x:192.6,alpha:1},0).wait(48).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(17).to({_off:false,regX:87.3,regY:31.9,x:265.3,y:35.3},0).wait(1).to({regX:35.1,regY:13.8,x:207.9,y:17.2,alpha:0.25},0).wait(1).to({x:202.8,alpha:0.5},0).wait(1).to({x:197.7,alpha:0.75},0).wait(1).to({x:192.6,alpha:1},0).wait(41));

	// Engineering
	this.instance_7 = new lib.Engineering();
	this.instance_7.parent = this;
	this.instance_7.setTransform(248.1,35.3,1,1,0,0,0,132.1,31.9);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(13).to({_off:false},0).wait(1).to({regX:52.9,regY:15.8,x:152.6,y:19.2,alpha:0.25},0).wait(1).to({x:136.3,alpha:0.5},0).wait(1).to({x:120,alpha:0.75},0).wait(1).to({x:103.8,alpha:1},0).wait(52).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(13).to({_off:false,regX:132.1,regY:31.9,x:248.1,y:35.3},0).wait(1).to({regX:52.9,regY:15.8,x:152.6,y:19.2,alpha:0.25},0).wait(1).to({x:136.3,alpha:0.5},0).wait(1).to({x:120,alpha:0.75},0).wait(1).to({x:103.8,alpha:1},0).wait(45));

	// Your
	this.instance_8 = new lib.Your();
	this.instance_8.parent = this;
	this.instance_8.setTransform(101.3,35.8,1,1,0,0,0,52.8,31.9);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(9).to({_off:false},0).wait(1).to({regX:21.4,regY:14.2,x:59.9,y:17.9,alpha:0.25},0).wait(1).to({x:49.9,y:17.8,alpha:0.5},0).wait(1).to({x:39.9,y:17.7,alpha:0.75},0).wait(1).to({x:29.9,y:17.6,alpha:1},0).wait(56).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).to({_off:true},1).wait(9).to({_off:false,regX:52.8,regY:31.9,x:101.3,y:35.8},0).wait(1).to({regX:21.4,regY:14.2,x:59.9,y:17.9,alpha:0.25},0).wait(1).to({x:49.9,y:17.8,alpha:0.5},0).wait(1).to({x:39.9,y:17.7,alpha:0.75},0).wait(1).to({x:29.9,y:17.6,alpha:1},0).wait(49));

	// logo
	this.instance_9 = new lib.Symbol1();
	this.instance_9.parent = this;
	this.instance_9.setTransform(109.5,85.5,1,1,0,0,0,87.5,33.5);
	this.instance_9.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1).to({regX:245.9,regY:-26.6,x:267.9,y:25.4,alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(65).to({alpha:0.8},0).wait(1).to({alpha:0.6},0).wait(1).to({alpha:0.4},0).wait(1).to({alpha:0.2},0).wait(1).to({alpha:0},0).wait(1).to({regX:87.5,regY:33.5,x:109.5,y:85.5},0).wait(1).to({regX:245.9,regY:-26.6,x:267.9,y:25.4,alpha:0.25},0).wait(1).to({alpha:0.5},0).wait(1).to({alpha:0.75},0).wait(1).to({alpha:1},0).wait(58));

	// Backgound
	this.instance_10 = new lib.GSC1879_ABMBannerAd_320x50_bg();
	this.instance_10.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(136));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(160,25,320,50);
// library properties:
lib.properties = {
	id: 'D4C83844DFB7CD4CA2819D590FAC090D',
	width: 320,
	height: 50,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/GSC1879_ABM_Banner_Ad_320x50_atlas_.png", id:"GSC1879_ABM_Banner_Ad_320x50_atlas_"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['D4C83844DFB7CD4CA2819D590FAC090D'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;